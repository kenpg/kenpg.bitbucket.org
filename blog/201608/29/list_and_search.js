window.addEventListener('load', seesaa_all);

function seesaa_all() {
    var tid = 'old_posts',
        cnt = 0;
    var ts = setInterval(function() {
        var tbl = document.getElementById(tid),
            obj = document.createElement('object');
        cnt++;
        if (tbl && obj) {

            while(tbl.hasChildNodes())
                tbl.removeChild(tbl.firstChild);
            tbl.innerHTML = '<tr><th>Date'
                + '<th>Title & Link'
                + '<i class="icon-external-link"></i>'
                + '<span id="toggle_links"></span>'
                + '<th>Tags<span id="tb_loading">loading...</span>';

            clearInterval(ts);
            with(obj) {
                onload = function(e) { tsv2tbl(e) };
                type = 'text/plain';
                setAttribute('tableid', tid);
                data = tbl.getAttribute('data');
            }
            document.body.appendChild(obj);
        } else if (cnt === 10) {
            // timeout
            document.getElementById('tb_loading')
                .textContent = '想定外エラーで読み込めません m(_)m';
        }
    }, 100);
}

function tsv2tbl(e) {
    var o = e.target,
        rows = o.contentDocument.body.textContent.split(/[\n\r]+/),
        nrow = rows.length,
        tbl = document.querySelector(
            '#' + o.getAttribute('tableid') + ' tbody');
    
    for (var i = 0; i < nrow; i++) {
        if (rows[i].length === 0) break;
        var cols = rows[i].split(/\t/),
            url_html = cols.splice(1, 1)[0],
            url_pdf = 'kenpg.php.xdomain.jp/' + cols[0] + '_'
                + url_html.match(/(\d+)\.html$/)[1] + '.pdf'
            tr = document.createElement('tr');
        cols[1] = '<a html="//' + url_html + '" pdf="//' + url_pdf
            + '" target="_blank">' + cols[1] + '</a>';
        tr.innerHTML = '<td>' + cols.join('<td>');
        tbl.appendChild(tr); 
    }
    o.parentNode.removeChild(o);
    document.getElementById('tb_loading').style.display = 'none';
    document.getElementById('search_wrap').style.display = 'block';

    document.getElementById('toggle_links').innerHTML
        = 'PDF<table>'
        + '<td style="background: lightblue">'
        + '<td>'
        + '<td onclick="toggle_link()" style="background: white; cursor: pointer">'
        + '</table>HTML';
//    set_links(false);
      set_links(true);
}

function toggle_link() {
    var tds = document.querySelectorAll('#toggle_links td'),
        td1 = tds[0],
        td2 = tds[2],
        st1 = td1.style,
        st2 = td2.style,
        bg1 = getComputedStyle(td1).backgroundColor,
        bg2 = getComputedStyle(td2).backgroundColor,
        pdf = td1.onclick == null,
        fnc = pdf ? td2.onclick : td1.onclick;

    st1.backgroundColor = bg2;
    st2.backgroundColor = bg1;
    st1.cursor = pdf ? 'pointer' : '';
    st2.cursor = pdf ? '' : 'pointer';
    td1.onclick = pdf ? fnc : '';
    td2.onclick = pdf ? '' : fnc;
    set_links(! pdf);
}

function set_links(flag_pdf) {
    var links = document.querySelectorAll('#old_posts a');
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        link.href = link.getAttribute(flag_pdf ? 'pdf' : 'html');
    }
}

// todo: 改行でも機能させる
// todo: テキスト入りでもまぁまぁ速くなった（removing count.php）ので使う
// ただし全箇所でなく上限設けて. 次回でも可
function seesaa_search() {
    try {
        var str = document.querySelector('#main input').value;
    } catch (e) {
    }
    if (!str)
        return;
    var url = 'http://kenpg.php.xdomain.jp/seesaa_search_simple.php?' + str,
        xhr = new XMLHttpRequest;
    xhr.onload = function() { disp_search_res(this.responseText) };
    xhr.open('GET', url);
    xhr.send(null);
    document.querySelector('#search_wrap i').style.display = 'inline-block';
}

function disp_search_res(text) {
    document.querySelector('#search_wrap i').style.display = 'none';

    if (! text) {
        // show message
        return;
    }

    var json = JSON.parse(text),
        ymds = [],
        hits = [];
    if (! (json instanceof Array) && json.hasOwnProperty('message')) {
        console.log(json.message);
        json = [];
        // return;
    }
    json.forEach(function(obj) {
        ymds.push(obj.ymd);
        hits.push(obj.hits);
    });

    var trs = document.querySelectorAll('#old_posts > tbody > tr')
    for (var i = 0; i < trs.length; i++) {
        var tr = trs[i],
            tds = tr.childNodes,
            ncol_plus = tds.length === 3;
        if (i === 0 && ncol_plus) {
            var th = document.createElement('th');
            th.textContent = 'Hits';
            th.style.paddingRight = '0.25em';
            tr.insertBefore(th, tds[1])
        } else if (i > 0) {
            var pos = ymds.indexOf(tr.childNodes[0].textContent),
                hit = pos >= 0;
            tr.className = hit ? 'hit' : 'hidden';
            if (hit) {
                if (ncol_plus) {
                    var td = document.createElement('td');
                    tr.insertBefore(td, tds[1])
                }
                var t = tds[1],
                    s = t.style;
                t.textContent = hits[pos];
                s.paddingRight = '0.25em';
                s.textAlign = 'right';
            }
        }
    }
    tr_hits = document.querySelectorAll('tr.hit');
    var mAry = [];
    for (var i = 0; i < tr_hits.length; i++) {
        var tds = tr_hits[i].childNodes;
        mAry.push([ tds[1].textContent, tds[0].textContent, i]);
    }
    mAry.sort(custom_sort);
    var fg = document.createDocumentFragment();
    mAry.forEach(function(ary) {
        fg.appendChild(tr_hits[ ary[2] ]);
    });
    document.querySelector('#old_posts > tbody').appendChild(fg);
}

function custom_sort(a, b) {
    if (parseInt(a[0]) < parseInt(b[0])) {
        return 1;
    } else if (a[0] === b[0]) {
        if (a[1] < b[1])
            return 1;
        else if (a[1] === b[1])
            return 0;
        else
            return -1;
    } else {
        return -1;
    }
}

function init_inp(elem) {
    var s = elem.style;
    if (s.color !== 'black') {
        s.color = 'black';
        elem.value = '';
    }
}

function key_inp(evt) {
    // return; // temporally
    if ((evt.which || evt.keyCode) === 13)
        seesaa_search();
}
