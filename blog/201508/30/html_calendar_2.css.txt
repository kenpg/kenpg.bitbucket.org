body {
    background: whitesmoke;
    margin: 0;
    padding: 0;
}

c {
    background: white;
    display: block;
    font-size: 11pt;
    font-family: Cambria;
    padding: 1.25em 1em 0 2em;
    page-break-after: always;
    overflow: hidden;
    height: calc(209.9125mm - 1.25em);
    width: calc(148.5mm - 1em - 2em) ; /* A5 */
}

.year.month {
    font-size: 1.5em;
    margin-bottom: 1em;
}

.days {
    border-top: solid black 1px;
    padding-left: 0.25em;
}

.days.endOfWeek {
    border-bottom: solid black 1px;
}

.days:last-child {
    border-bottom: solid black 1px;
}

@media print {
    body {
        background: white; 
    }
}
