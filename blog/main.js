/* codig: utf-8 */

// adjust space between ascii and Japanse character
// in reference to http://www15.big.or.jp/~nmajima/mt/2008/03/post_170.html
kob.adjustSpace = function(text) {
    var wide1 = '([^\n\!-~｡-ﾟ （）「」＋：＆■・、。])',
        wide2 = '([^\n\!-~｡-ﾟ （）「」＋：＆■・、。])',
        half  = '([\!-~｡-ﾟ])',
        after = '$1<s1></s1>$2';
    var r1 = new RegExp(wide1 + ' *' + half, 'g'),
        r2 = new RegExp(half + ' *' + wide2, 'g');
    return text.replace(r1, after).replace(r2, after);
};

// prepare body and store article HTML
// TODO: 'loading' must be showed more previously.
(function () {
    var d = [];
    for (i = 0; i < 5; i++) d.push(kob.ce('div'));
    d[0].id = 'top';
    d[1].id = 'header';
    d[2].id = 'main';
    d[3].id = 'footer';
    d[4].id = 'right';
    d[0].className = d[3].className = d[4].className = 'nocontent';

    var a = kob.ce('a'),
        i = kob.ce('img'),
        s = kob.ce('span'),
        hi = kob.ce('div'),
        ld = kob.ce('div');
        hr = kob.ce('hr');

    a.href = kob.pth || '/';
    i.id = 'logo';
    i.src = kob.pth + 'img/logo/20150418_logo.gif';
    s.id = 'top_link';
    hi.id = 'header_in';
    if (!kob.idx)
        d[2].lang = 'ja';
    ld.id = 'loading';

    kob.ap(a, i);
    kob.ap(d[0], a);
    kob.ap(d[0], s);
    kob.ap(d[1], hi);
    kob.tt(ld, 'Loading...');
    kob.ap(d[3], hr);
    kob.ap(d[3], ld);

    var fg = document.createDocumentFragment(),
        bdy = document.body;

    for (i = 0; i < 5; i++)
        kob.ap(fg, d[i]);
    if (!kob.idx) {
        var fg2 = document.createDocumentFragment(),
            chd = bdy.childNodes,
            len = chd.length;
        for (var i = 0; i < len; i++) {
            var c = chd[i],
                t = c.tagName;
            if (typeof t !== 'undefined' && t.match(/script/i)) {
                if (typeof c.src === 'string' &&
                    c.src.match(/kenpg_ver1_index\.js$/))
                        continue;
            }
            kob.ap(fg2, chd[i].cloneNode(true));
        }
        while(bdy.firstChild)
            bdy.removeChild(bdy.firstChild);
        kob.articleFgm = fg2;
    }
    var chd = fg.childNodes,
        len = chd.length;
    for (var i = 0; i < len; i++)
        kob.ap(bdy, chd[i].cloneNode(true));

    if (bdy.style.visibility === 'hidden')
        bdy.style.visibility = 'visible';
    kob.rm(kob.id('loading'));
})();

// top links
(function () {
    var obj = {
        'Home' : '/',
        "This blog's repository"
            : 'https://bitbucket.org/kenpg/kenpg.bitbucket.org/',
        'Bitbucket' : 'https://bitbucket.org/kenpg/' },
         s = kob.id('top_link');
    // "Object.keys" isn't available on IE8 and before
    for (var k in obj) {
        var a = kob.ce('a');
        a.href = obj[k];
        kob.tt(a, k);
        if (k !== 'Home')
            kob.tt(s, '|');
        kob.ap(s, a);
    }
//    s.innerHTML = '<a class="atom" href="http://kenpg.php.xdomain.jp/atom.xml" target="_blank">'
  //      + '<i class="icon-rss-sign"></i></a>' + s.innerHTML;
})();

// top small image link new
(function () {
    var url = 'http://kenpg2.seesaa.net',
        d1 = kob.ce('div'),
        a1 = kob.ce('a'),
        d2 = kob.ce('div'),
        a2 = kob.ce('a'),
        im = kob.ce('img');

    d1.id = 'oldsite_wrap';
    d2.id = 'oldsite_div';
    im.id = 'oldsite_img';
    im.src = kob.pth + 'img/logo/201304_201504_small.png';

    a1.href = a2.href = url;
    kob.tt(a1, ' 2013.4 »» 2015.4');
    kob.br(a1);
    kob.ap(a2, im);
    kob.ap(d2, a2);
    kob.ap(d1, a1);
    kob.ap(d1, d2);
    kob.ap(kob.id('top'), d1);
})();

// search box
(function () {
    if (kob.ua.match(/MSIE 6/)) return; // unable to locate properly

    var f = kob.ce('form');
    f.id = 'search';
    f.action = kob.url_pref + 'google.com/cse';
    f.innerHTML = [
        '<input id="search_text" name="q" type="text" value="">',
        '<input id="search_submit" name="sa" type="submit" value="Search">',
        '<input name="cx" type="hidden">',
        '<input name="ie" type="hidden" value="UTF-8">',
    ].join('');
    f.onsubmit = function () {
        if (kob.local || kob.test) return;
        ga('send', 'event', 'search_custom', f['q'].value);
    };
    f.target = '_blank';
    f['q'].onblur  = function (e) {
        var t = e.target ||  window.event.srcElement;
        if (typeof t === 'undefined') return; // just in case
        t.style.color = 'silver'
    };
    f['q'].onfocus = function (e) {
        var t = e.target ||  window.event.srcElement;
        if (typeof t === 'undefined') return; // just in case
        if (t.value === 'ブログ内検索 by Google') t.value = '';
        t.style.color = 'black'
    };
    f['q'].value  = 'ブログ内検索 by Google';
    f['cx'].value = '017835495400266567830:equgjrq2joy';

    if (kob.idx) {
        kob.ap(kob.id('header'), f);
    } else {
        var r = kob.id('right');
        r.insertBefore(f, r.firstChild);
    }
})();

// build right sidebar
(function () {
    /*|
    |*| changed 'thanks' to be omitted 
    |*/
    var ids = ['r_recent', 'r_tag', 'r_link', 'r_summary'],
        subs = ['Recent Posts', 'Tags', 'Official sites & docs', 'About'],
        div = kob.ce('div'),
        r = kob.id('right');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i],
            subtitle = subs[i];
        if (subtitle) {
            var d2 = div.cloneNode();
            d2.className = 'r_subtitle';
            kob.tt(d2, subtitle);
            kob.ap(r, d2);
        }
        var d1 = div.cloneNode()
        d1.id = id;
        kob.ap(r, d1);
    }
})();

// summary
(function () {
    var d1 = kob.id('r_summary'),
        div = kob.ce('div'),
        lnk = kob.ce('a');

    kob.tt(d1, 'kenpg is an abbreviation for "kenkyu" (means research in Japanese) and postgres.');
    kob.br(d1, 2);
    kob.tt(d1, 'Main topics of this blog are how I use postgres, PostGIS, R and so on.');
    kob.br(d1, 2);
    lnk.href = kob.pth + 'blog/201505/01.html';
    kob.tt(lnk, '» about and contact');
    kob.ap(div, lnk);
    kob.ap(d1, div);
})();

// Thanks to
/*|
|*| changed to be omitted 
|*/
(function () {
    var d1 = kob.id('r_thanks');
    if (! d1)
        return;
    var d2 = kob.ce('div'),
        links = [
        'AutoHotkey @ www.autohotkey.com/',
        'Auto Reload (Fx Ext.) @ addons.mozilla.org/en-US/firefox/addon/auto-reload/',
        'Bitbucket @ bitbucket.org/',
        'ConEmu @ conemu.github.io/',
        'FFmpeg @ www.ffmpeg.org/',
        'Font Awesome @ fortawesome.github.io/Font-Awesome/',
        'Git Portable @ github.com/sheabunge/GitPortable',
        'Google Custom Search @ cse.google.com/',
        'Mery @ www.haijin-boys.com/wiki/%E3%83%A1%E3%82%A4%E3%83%B3%E3%83%9A%E3%83%BC%E3%82%B8',
        'Mozilla Firefox @ www.mozilla.org/firefox/' ];

    kob.tt(d1, 'Software and services below are the essentials of the creating this blog. They have helped me a lot.');
    for (var i = 0, len = links.length; i < len; i++) {
        if (links[i] !== '') {
            var s = links[i].split(' @ '),
                a = kob.ce('a');
            a.href = kob.url_pref + s[1];
            kob.tt(a, '» ');
            kob.tt(a, s[0]);
            kob.ap(d2, a);
        }
        kob.br(d2);
    }
    kob.ap(d1, d2);
})();

// recent
kob.showMoreRecent = function (top) {
    var d1 = kob.id('r_recent');
    if (! d1)
        return; // unexpected

    var ids = ['r_recent_ul', 'r_recent_next', 'r_recent_prev'],
        ul = kob.id(ids[0]),
        a_next = kob.id(ids[1]),
        a_prev = kob.id(ids[2]),
        lim = 5;

    if (typeof top === 'number' && ul && a_next && a_prev) {
        // Don't use "!top". It excludes "top === 0"
        while(ul.hasChildNodes()) ul.removeChild(ul.firstChild);
    } else if (!top && !ul && !a_next && !a_prev) {
        top = 0;
        ul = kob.ce('div');
        ul.id = ids[0];
        
        var a = kob.ce('a');
        a.href = 'javascript:void(0)';
        a_next = a.cloneNode(),
        a_prev = a.cloneNode();
        a_next.id = ids[1];
        a_prev.id = ids[2];
        kob.tt(a_next, '» Next');
        kob.tt(a_prev, '» Prev');

        kob.ap(d1, ul);
        kob.ap(d1, a_next);
        kob.ap(d1, a_prev);
    } else {
        return; // unexpected
    }

    var plen = kob.post.length,
        li = kob.ce('li'),
        lk = kob.ce('a'),
        sp = kob.ce('span'),
        flag_end = false,
        ymd1 = '',
        ymd2 = '';

    for (var i = 0; i < lim; i++) {
        var num = plen - top - i -1;
        if (num < 0) {
            flag_end =true;
            break;
        }
        var ob = kob.post[num],
            lab = kob.adjustSpace(ob[1]),
            li2 = li.cloneNode(),
            lk2 = lk.cloneNode(),
            ymd_div = kob.ce('div');

        ymd_div.className = "r_recent_ymd";
        ob[0] = String(ob[0]);
        ymd1 = ob[0].replace(/^(\d{6})(\d{2})$/, '$1/$2');
        ymd2 = ob[0].replace(/^(\d{4})(\d{2})(\d{2})$/, '$1.$2.$3');
        lk2.href = kob.pth + 'blog/' + ymd1 + '.html';
        kob.tt(ymd_div, ymd2.replace(/\.0/g, '.'));
        lk2.innerHTML = lab;

        kob.ap(li2, ymd_div);
        kob.ap(li2, lk2);
        kob.ap(ul, li2);
    }
    if (!flag_end)
        a_next.onclick = function() {
            kob.showMoreRecent(top + lim);
            kob.adjustRecentPosts();
        };
    if (top - lim >= 0) {
        a_prev.onclick = function() {
            kob.showMoreRecent(top - lim);
            kob.adjustRecentPosts();
        };
        a_prev.className = '';
    } else {
        a_prev.className = 'nodisp';
    }
};
kob.showMoreRecent();

// tags
(function () {
    var main_tags = ['PostgreSQL', 'PostgreSQL 9.5', 
            'PL/Python', 'PL/R', 'PL/v8',
            'PostGIS', 'pgAdmin', 'pgAdmin4', 'psql'
        ];

    var other_tags = [];
    for (var i = 0; i < kob.post.length; i++) {
        var ary = kob.post[i][2];
        for (var j = 0; j < ary.length; j++) {
            var k = ary[j];
            if (main_tags.indexOf // IE8 not available
                    && main_tags.indexOf(k) < 0 && other_tags.indexOf(k) < 0)
                other_tags.push(k);
        }
    }
    other_tags.sort( function(a, b) {
        if (a.toLowerCase() < b.toLowerCase())
            return -1;
        else if (a.toLowerCase() > b.toLowerCase())
            return 1;
        else 
            return 0;
    });

    var div1 = kob.ce('div'),
        div2 = div1.cloneNode(),
        lnk = kob.ce('a'),
        len_main = main_tags.length;
    div1.id = 'r_tags_main';
    div2.id = 'r_tags_others';
    lnk.className = 'r_tag_links';
    var tags = main_tags.concat(other_tags);
    for (var i = 0; i < tags.length; i++) {
        var tag = tags[i],
            ln = lnk.cloneNode(),
            target = i < len_main ? div1 : div2;
        ln.href = kob.pth + '?word=' + encodeURIComponent(tag);
        kob.tt(ln, tag);
        kob.ap(target, ln);
    }

    var par = kob.id('r_tag'),
        a = kob.ce('a');

    a.id = 'toggleTags';
    a.href = 'javascript:void(0)';
    a.onclick = function (e) { toggle_tags(e) };
    kob.tt(a, '» Show Others');
    kob.ap(par, div1);
    kob.ap(par, a);
    kob.ap(par, div2);
})();

function toggle_tags(e) {
    var lnk = e.target;
        tags = kob.id('r_tag').childNodes,
        len = tags.length;
    if (lnk.textContent.match(/show/i)) {
        kob.addstyle({ '#r_tags_others': ['display: block'] });
        lnk.textContent = lnk.textContent.replace(/show/i, 'Hide');
    } else {
        kob.addstyle({ '#r_tags_others': ['display: none'] });
        lnk.textContent = lnk.textContent.replace(/hide/i, 'Show');
    }
}

// links
(function () {
    var d3 = kob.ce('div'),
        ary = [
        'PostgreSQL @ www.postgresql.org/',
        'PostgreSQL : JP @ www.postgresql.jp/',
        'PostgreSQL : JP doc @ www.postgresql.jp/document/',
        'PostGIS @ postgis.net/',
        'PostGIS : JP doc @ www.finds.jp/docs/pgisman/',
        'pgAdmin @ www.pgadmin.org/',
        'pgRouting @ pgrouting.org/',
        'PL/R @ www.joeconway.com/plr/',
        'PL/v8 @ pgxn.org/dist/plv8/' /*,
        'MADlib @ madlib.net/',
        '',
        'CentOS @ www.centos.org/',
        'Python @ www.python.org/',
        'R @ cran.r-project.org/',
        'Qt @ qt-project.org/',
        'Qt : JP @ qt-users.jp/',
        '',
        'FOSS4G 2015 @ 2015.foss4g.org/',
        'OpenStreetMap @ www.openstreetmap.org/',
        'OpenStreetMap : JP @ www.openstreetmap.jp/',
        'OSGeo @ www.osgeo.org/',
        'OSGeo : JP @ www.osgeo.jp/' */
    ];
    for (var i = 0; i < ary.length; i++) {
        var lnk = ary[i];
        if (lnk !== '') {
            var s = lnk.split(' @ '),
                a = kob.ce('a');
            a.href = kob.url_pref + s[1];
            kob.tt(a, '» ' + s[0]);
            kob.ap(d3, a);
        }
        kob.br(d3);
    }
    kob.ap(kob.id('r_link'), d3);
})();

// Google Analytics on real site except on local
(function () {
    if (kob.local || kob.test)
        return;
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        };
        i[r].l = 1 * new Date();
        a = s.createElement(o);
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m);
    })(window, document, 'script',
        kob.url_pref + 'www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-42334728-1', 'auto');
    ga('send', 'pageview');
})();

// add Atom Feed link and image to top
(function () {
    window.atom_func = function() {
        var atom_div = document.querySelector('#atom');
        if (document.querySelector('#main').offsetWidth < 480
            || document.querySelector('#right').offsetWidth == 0) {
            if (atom_div) {
                atom_div.parentNode.removeChild(atom_div);
            }
            return;
        } else {
            if (atom_div) {
                // console.log(new Date, 'exists');
                return;
            }
            atom_div = document.createElement('div');
            atom_div.id = "atom";
            atom_div.innerHTML = [
                '<a icon href="http://kenpg.php.xdomain.jp/atom.xml" ',
                ' target="_blank" title="Atom Feed">',
                '<i class="icon-rss-sign"></i></a>',
//                '<br><a href="http://www.feedvalidator.org/check.cgi?url=',
  //              'http%3A%2F%2Fkenpg.php.xdomain.jp%2Fatom.xml">',
    //            '<img src="/img/valid-atom.png"></a>'
                ].join('');
            var lnks = atom_div.getElementsByTagName('a');
            for (var i = 0; i < lnks.length; i++)
                lnks[i].target = "_blank";
            document.getElementById('top').appendChild(atom_div);
        }
    };
    atom_func();
    window.addEventListener('resize', atom_func);
})();

kob.common_end_for_ie = true;
