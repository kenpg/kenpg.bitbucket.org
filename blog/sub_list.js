/* coding: utf-8 */

// settings
kob.list_rows = 15;

// check show/hide images
(function() {
    kob.noimg = kob.loc.match(/[&\?]noimg$/) ? true : false;
    if (kob.noimg) {
        kob.addstyle({
            '.img_in_list_wrap': ['display: none']
        });
    } else {
        kob.addstyle({
            '.img_in_list_wrap + .week_delim': ['margin-top: -42px']
        });
    }
})();

// check tag and page
(function() {
    kob.tag = '';
    try { // avoid error by unexpected query word
        var m = kob.loc.match(/\?word=([-\.\w\%]+)/);
        if (m !== null) kob.tag = decodeURIComponent(m[1]);
    } catch (e) {
    }

    if (kob.tag !== '') {
        kob.post_tmp = [];
        for (var i = 0, len = kob.post.length; i < len; i++) {
            var p = kob.post[i];
            for (var j = 0, len2 = p[2].length; j < len2; j++) {
                if (kob.tag === p[2][j]) kob.post_tmp.push(p);
            }
        }
    } else {
        kob.post_tmp = kob.post;
    }
    kob.post_tmp_len = kob.post_tmp.length;

    // page
    var m = kob.loc.match(/[\?&]page=(\d+)/);
    kob.pno = m !== null ? parseInt(m[1], 10) : 1;

    var r = kob.list_rows,
        end = kob.post_tmp.length - r * (kob.pno - 1),
        start = end < r ? 0 : end - r;
    kob.post_tmp = kob.post_tmp.slice(start, end);
})();

// page title
(function() {
    var title = '';
    if (kob.tag === '' && kob.pno === 1)
        title = 'kenpg - my research and PostgreSQL';
    else if (kob.tag === '' && kob.pno > 1)
        title = 'kenpg - list of articles (' + kob.pno + ')';
    else
        title = 'kenpg - ' + kob.tag + ' (' + kob.pno + ')';
    document.title = title;
})();

// header (pager)
(function() {
    var hin = kob.id('header_in'),
        d1 = kob.ce('div'),
        d2 = d1.cloneNode(),
        spn = kob.ce('span'),
        lnk = kob.ce('a'),
        url = '';

    d1.id = 'header_in_info';
    kob.tt(d1, kob.tag === '' ? 'List of articles' : 'Tag : ' + kob.tag);
    kob.ap(hin, d1);

    d2.id = 'header_in_pager';
    kob.tt(d2, 'Page :');

    for (var i = 1; i <= Math.ceil(kob.post_tmp_len / kob.list_rows); i++) {
        var a = (i === kob.pno) ? spn.cloneNode() : lnk.cloneNode();
        a.className = 'header_in_pageNum';
        if (i !== kob.pno) {
            if (kob.tag === '')
                url = '/' + (i > 1 ? '?page=' + i : '');
            else
                url = '?word=' + encodeURIComponent(kob.tag)
                    + (i > 1 ? '&page=' + i : '');
        }
        if (kob.noimg)
            url += (url.match(/\?/) ? '&' : '?') + 'noimg';
        a.href = url;
        kob.tt(a, i);
        kob.ap(d2, a);
        kob.ap(d2, kob.ce('wbr'));
    }
    kob.ap(hin, d2);
})();

// footer pager
(function() {
    if (kob.cl('header_in_pageNum').length < 2) {
        kob.addstyle({'#footer':['display: none']});
        return;
    }
    var c = kob.id('header_in_pager').cloneNode(true);
    c.id = 'footer_pager';
    if (c.getElementsByTagName('a').length === 0) return;
    var f = kob.id('footer');
    kob.ap(f, c);
})();

function images_sort_and_last(ary) {
    if (!ary)
        return;
    else if (ary.length === 1)
        return ary[0];

    ary.sort(function(s1, s2) {
        var n1 = s1.match(/^\d+/),
            n2 = s2.match(/^\d+/);
        if (n1 && n2) {
            var t1 = parseInt(n1[0]),
                t2 = parseInt(n2[0]);
        } else {
            var t1 = s1.toLowerCase(),
                t2 = s2.toLowerCase();
        }
        if (t1 < t2)
            return 1;
        else if (t1 === t2)
            return 0;
        else
            return -1;
    });
    return ary[0];
}

// article list
(function() {
    var d = kob.ce('div'),
        a = kob.ce('a'),
        s = kob.ce('span'),
        s1 = s.cloneNode(),
        s2 = s.cloneNode();
    d.className = 'article_list';
    s1.className = 'article_date';
    s2.className = 'article_tags';

    for (var i = kob.post_tmp.length - 1; i >= 0; i--) {
        var p = kob.post_tmp[i],
            ymd = p[0].toString(),
            div = d.cloneNode(),
            lnk = a.cloneNode(),
            dt = s1.cloneNode(),
            tags = [];
        kob.tt(dt, kob.ymd_sep(ymd));
        kob.ap(div, dt);
        div.setAttribute('ymd', ymd);
        
        var imgsrc = images_sort_and_last(p[3]);
        if (imgsrc)
            div.setAttribute('img', imgsrc);
        if (i % 2 === 0)
            div.className += ' bgcolor';

        // article title
        lnk.href = 'blog/' + ymd.substring(0, 6) +
            '/' + ymd.substring(6, 8) + '.html';
        lnk.innerHTML = kob.adjustSpace(p[1]);
        kob.ap(div, lnk);

        // tags
        var tags = p[2],
            tagsNum = tags.length;
        if (tagsNum > 0) {
            var spn = s2.cloneNode();
            kob.tt(spn, '[ ');
            tags.forEach(function(tag, i) {
                var lnk2 = kob.ce('a');
                kob.tt(lnk2, tag);
                lnk2.href = kob.pth + '?word=' + encodeURIComponent(tag);
                kob.ap(spn, lnk2);
                if (i < tagsNum - 1)
                    kob.tt(spn, ' / ')
            });
            kob.tt(spn, ' ]');
            kob.ap(div, spn);
        }
        kob.ap(kob.id('main'), div);
    }
})();

// insert delimiter of week
(function() {
    var lis = kob.cl('article_list'),
        preNum = null;
        
    for (var i = 0, len = lis.length; i < len; i++) {
        var div = lis[i],
            str = div.firstChild.firstChild.nodeValue;

        if (kob.tag !== '' && (i === 0 || i % 7 !== 0))
            continue;
    
        var wday = (new Date(str)).getDay();
        if (kob.tag === '' && (i === 0 || wday !== 0))
            continue;
    
        var m = kob.ce('img');
        m.className = 'week_delim';
        m.src = kob.pth + 'img/32x32_'
            + ((Math.round(i / 7) + 1) % 3 + 1) + '.gif';
        div.parentNode.insertBefore(m, div);
        kob.addstyle({
           '.img_in_list_wrap + .week_delim': [
               'display: ' + (kob.noimg ? 'inline' : 'none'),
               'margin-top: 10px']
       });
    }
})();

kob.toggleImages = function(e, show) {
    kob.addstyle({
        '.img_in_list_wrap': [
            'display:' + (show ? 'inline-block' : 'none')],
        '.img_in_list_wrap + .week_delim': [
            'display:' + (show ? 'none' : 'inline')]
    });

    var que = 'noimg',
        reg = new RegExp('[&\?]' + que),
        links = kob.tg('a'),
        len = links.length;
    for (var i = 0; i < len; i++) {
        var lnk = links[i],
            url = lnk.href;
        if (url.match(/^javascript/))
            continue;
        else if (show)
            lnk.href = url.replace(reg, '');
        else
            lnk.href += (url.match(/\?/) ? '&' : '?') + que;
    }

    var t = e ? e.target : window.event.srcElement;
    if (typeof t === 'undefined')
        return; // just in case
        
    t.firstChild.nodeValue = (show ? 'Hide' : 'Show') + ' Images';
    t.onclick = function(e) {
        kob.toggleImages(e, show ? false : true)
    };
    kob.setHeight();
};

// add images for IE9-
(function() {
    if (kob.ua.match(/MSIE [78]/))
        return;
    kob.as('data_thumbs.js', function () {
        var posts = kob.cl('article_list'),
            cnt = 0;
        for (var i = 0; i < posts.length; i++) {
            var div = posts[i],
                ymd = div.getAttribute('ymd'),
                url_pre = 'blog/' + ymd.replace(/^(\d{6})(\d{2})$/, '$1/$2/'),
                imgsrc = '';
            if (kob.thumbs) {
                var tmp = kob.thumbs[ymd];
                if (tmp)
                    imgsrc = url_pre + tmp;
            }
            if (! imgsrc) {
                var tmp = div.getAttribute('img');
                if (tmp)
                    imgsrc = url_pre + tmp;
            }
            if (imgsrc) {
                cnt++;
                var im = new Image();
                im.setAttribute('pos', i);
                im.onload = function(e) {
                    var im = e ? e.target : window.event.srcElement,
                        div = kob.cl('article_list')[im.getAttribute('pos')],
                        a = kob.ce('a'),
                        w = Math.min(im.width, 200),
                        h = Math.min(w / im.width * im.height, 150);
                    a.className = 'img_in_list_wrap';
                    a.href = div.getElementsByTagName('a')[0].href;
                    a.style.backgroundImage = 'url(' + im.src + ')';
                    a.style.width = w + 'px';
                    a.style.height = h + 'px';
                    div.parentNode.insertBefore(a, div.nextSibling);
                };
                im.src = imgsrc;
            }
        }
        if (cnt > 0) {
            var a = kob.ce('a');
            a.id = 'img_toggle';
            a.href = 'javascript:void(0)';
            a.onclick = function(e) { kob.toggleImages(e, kob.noimg) };
            kob.tt(a, (kob.noimg ? 'Show' : 'Hide') + ' Images');
            kob.ap(kob.id('header_in_pager'), a);
        }
    });
})();
