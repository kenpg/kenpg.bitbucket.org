var BinTree = function (ary) {

    // function for simplicity
    this.log2 = function (x) {
        return Math.log(x) / Math.log(2)
    };
    this.setAttr = function (obj, ary) {
        for (var i = 0; i < ary.length; i += 2) {
            obj.setAttribute(ary[i], ary[i + 1]);
        }
    };

    var x = [],
        y = [],
        len = ary.length,
        dep = Math.ceil(this.log2(len));

    // calculate coordinates
    for (var i = 0; i < len; i++) {
        var d1 = Math.floor(this.log2(i + 1)),
            d2 = i + 1 - Math.pow(2, d1);
        x[i] = Math.pow(2, dep - d1 - 2) * (2 * d2 + 1) + 0.5;
        y[i] = d1;
    }
    this.x = x;
    this.y = y;
    this.val = ary;
};

BinTree.prototype.drawNodes = function (rx, ry) {
    if (!ry) ry = rx;

    var svg = document.documentElement,
        ns = svg.namespaceURI,
        el = document.createElementNS(ns, 'ellipse'),
        x = this.x,
        y = this.y,
        mg = this.margin || 0,
        mx = rx + mg,
        my = ry + mg,
        x1 = Math.min.apply(null, x),
        y1 = Math.min.apply(null, y),
        ww = Math.max.apply(null, x) - x1 + 2 * mx,
        hh = Math.max.apply(null, y) - y1 + 2 * my,
        viewBox = (x1 - mx) + ' ' + (y1 - my) + ' ' + ww + ' ' + hh;
        val = this.val;

    svg.setAttribute('viewBox', viewBox);
    this.setAttr(el, ['rx', rx, 'ry', ry]);
    for (var i = this.x.length - 1; 0 <= i; i--) {

        // add lines
        if (i > 0) {
            var j = Math.floor((i + 1) / 2) - 1,
                l = document.createElementNS(ns, 'line');
            this.setAttr(l, [ 'x1', x[i], 'x2', x[j], 'y1', y[i], 'y2', y[j] ]);
            svg.appendChild(l);
        }

        // add ellipses
        var e = el.cloneNode();
        this.setAttr(e, [ 'cx', x[i], 'cy', y[i] ]);
        svg.appendChild(e);

        // add values
        var t = document.createElementNS(ns, 'text');
        this.setAttr(t, [ 'x', x[i], 'y', y[i],
            'font-size', (ry + 0.1) + 'px',
            'dy', (ry + 0.1) + 'em' ]);
        t.appendChild(document.createTextNode(val[i]));
        svg.appendChild(t);
    }
};

(function () {
    // generate a array of random integers
    var len = 25,
        ary = [];
    for (var i = 0; i < len; i++) ary[i] = Math.floor(Math.random() * 100);
    ary.sort(function(a, b) {
        return parseInt(a) < parseInt(b) ? 1 : -1;
    });

    // create Binary Tree and plot it
    var b1 = new BinTree(ary);
    b1.margin = 0.2;
    b1.drawNodes(0.3);
})();
