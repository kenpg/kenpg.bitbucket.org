function drawDiagonal(opt) {
    var d1 = document.getElementById('d1'),
        b3 = document.getElementById('b3');
    try {
        var r = d1.getBoundingClientRect();
    } catch(e) {
        var message = 'ブラウザが getBoundingClientRect() に未対応なので何もしません';
        d1.appendChild(document.createElement('br'));
        d1.appendChild(document.createTextNode(message));
        return;
    }
    var d2 = document.createElement('diagonal'),
        s2 = d2.style,
        tan = r.height / r.width * (opt === 'rightUp' ? -1 : 1),
        org = opt === 'rightUp' ? '100%' : 0,
        rndColor = 'rgb(' + 
            Math.floor(Math.random() * 255) + ', ' +
            Math.floor(Math.random() * 255) + ', ' +
            Math.floor(Math.random() * 255) + ')';

    d2.className = 'for_diagonal';
    s2.height = r.height + 'px';
    s2.width = r.width + 'px';
    s2.transformOrigin = org;
    s2.transform = 'matrix(1, ' + tan + ', 0, 1, 0, 0)';
    s2.borderColor = rndColor;
    d1.appendChild(d2);
    b3.style.display = 'block';
}

function clearDiagonal(btn) {
    var ds = document.getElementsByTagName('diagonal');
    while(ds.length > 0) {
        var d = ds[0];
        d.parentNode.removeChild(d);
    }
    btn.style.display = 'none';
}
