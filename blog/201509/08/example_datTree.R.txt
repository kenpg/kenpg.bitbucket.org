# create vector
vec = floor(runif(31) * 100)
vec = sort(unique(vec), decreasing=T)

# add coordinates of binary tree to each element
datTree = function (vec, order='') {
    len = length(vec)
    dep = ceiling(log2(len))
    dat = data.frame(x=rep(NA, len), y=rep(NA, len), val=vec)
    for (i in 1:dep) {
        j = 2 ^ (i - 1) : min(len, 2 ^ i - 1)
        dat$x[j] = (2 ^ (dep - i - 1)) * (2 * (seq(j) - 1) + 1) + 0.5
        dat$y[j] = ifelse(order == 'rev', i, 1 - i)
    }
    return(dat)
}

dat = datTree(vec)
print(dat)
