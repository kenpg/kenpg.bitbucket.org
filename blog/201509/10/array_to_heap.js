var TestHeap = function (len) {
    this.ary = [];
    this.time = [0, 0];
    this.numComp = [0, 0];
    this.heap_res = [];
    this.heap_chk = [];
    while (len > 0) {
        this.ary.push(Math.round(Math.random() * len));
        len--
    }
};

TestHeap.prototype.build_pre = function () {
    this.heap = [];
    this.cnt = 0;
    this.start = (new Date).getTime();
};

TestHeap.prototype.build_after = function (num) {
    this.check(num);
    this.time[num] = (new Date).getTime() - this.start;
    this.numComp[num] = this.cnt;
    this.heap_res[num] = this.heap;
};

TestHeap.prototype.build1 = function () {
    this.build_pre();
    var len = this.ary.length;
    for (var i = 0; i < len; i++) {
        this.heap.push(this.ary[i]);
        this.arrange1(i);
    }
    this.build_after(0);
};

TestHeap.prototype.arrange1 = function (ord) {
    if (ord === 0) return;
    var h = this.heap,
        parent = Math.floor((ord + 1) / 2) - 1,
        vc = h[ord],    // value current
        vp = h[parent]; // value parent
    if (vc > vp) {
        h[ord] = vp;
        h[parent] = vc;
        this.arrange1(parent); // recursive
    }
    this.cnt++;
};

TestHeap.prototype.build2 = function () {
    this.build_pre();
    this.heap = this.ary.concat();
    var start = Math.floor(this.heap.length / 2) - 1;
    for (var i = start; 0 <= i; i--) this.arrange2(i);
    this.build_after(1);
};

TestHeap.prototype.arrange2 = function (ord) {
    var h = this.heap,
        below = 2 * (ord + 1) - 1,
        vc = h[ord],       // value current
        vb = h[below],     // value below
        vr = h[below + 1]; // value right below
    if (typeof vb === 'undefined') {
        return;
    } else if (typeof vr !== 'undefined' && vb < vr) {
        below++;
        vb = vr;
    }
    if (vc < vb) {
        h[ord] = vb;
        h[below] = vc;
        this.arrange2(below); // recursive
    }
    this.cnt += 2;
};

TestHeap.prototype.check = function (num) {
    var h = this.heap,
        start = Math.floor(h.length / 2) - 1;
    for (var i = start; 0 <= i; i--) {
        var below = 2 * (i + 1) - 1;
        if (h[i] < h[below] || h[i] < h[below + 1]) {
            this.heap_chk[num] = 'ERROR at ' + i;
            return;
        }
    }
    this.heap_chk[num] = 'OK';
};

TestHeap.prototype.print = function (chars) {
    var ary = this.ary.slice(0, chars),
        h1 = this.heap_res[0].slice(0, chars),
        h2 = this.heap_res[1].slice(0, chars),
        dots  = this.ary.length > chars ? ', ...' : '',
        text = [
            'number of elements (' + this.ary.length + ')<br>' +
                ary.join(', ') + dots,
            'type1<br>check (' + this.heap_chk[0] + '), ' + 
                'number of comparisons (' + this.numComp[0] + '),&ensp;' +
                'time (' + this.time[0] + 'ms)<br>' + h1.join(', ') + dots,
            'type2<br>check (' + this.heap_chk[1] + '), ' + 
                'number of comparisons (' + this.numComp[1] + '),&ensp;' +
                'time (' + this.time[1] + 'ms)<br>' + h2.join(', ') + dots
        ];
    document.write(text.join('<br><br>'));
};

var re1 = location.search.match(/len=(\d+)/);
    re2 = location.search.match(/print=(\d+)/),
    len = re1 ? re1[1] : 1000;
    chars = re2 ? re2[1] : Math.ceil(80 / (Math.log(len) / Math.log(10) + 1));
var test = new TestHeap(len);
test.build1();
test.build2();
test.print(chars);
