var TestHeap = function (len) {
    this.ary = [];
    this.heap_init = [];
    this.heap_sorted = [];
    this.record = { 'time': [], 'check': [] };
    while (len > 0) {
        this.ary.push(Math.round(Math.random() * len));
        len--
    }
};

TestHeap.prototype.build2 = function () {
    this.start = (new Date).getTime();
    var h = this.ary.concat(),
        len = h.length,
        ord_start = Math.floor(len / 2) - 1;
    for (var i = ord_start; 0 <= i; i--) this.arrange3(i, len, h);
    this.heap_init = h;
    this.record.time[1] = (new Date).getTime() - this.start;
};

TestHeap.prototype.sort1 = function () {
    this.start = (new Date).getTime();
    this.heap_sorted = this.heap_init.concat();
    var h1 = this.heap_init.concat(), h2 = this.heap_sorted,
        len = h1.length - 1, last = null;
    for(var i = 0; i <= len; i++) {
        h2[i] = h1[0]; // copy max value
        if (i === len) break;
        last = len - i;
        h1[0] = h1[last];
        this.arrange3(0, last, h1);
    }
    this.record.time[2] = (new Date).getTime() - this.start;
};

TestHeap.prototype.arrange3 = function (ord, last, h) {
    var below = 2 * (ord + 1) - 1;
    if (last < below) return;

    var vc = h[ord],       // value current
        vb = h[below],     // value below
        vr = h[below + 1]; // value right below
    if (below < last && vb < vr) {
        below++;
        vb = vr;
    }
    if (vc < vb) {
        h[ord] = vb;
        h[below] = vc;
        this.arrange3(below, last, h); // recursive
    }
};

TestHeap.prototype.check = function () {
    var h1 = this.heap_init,
        start = Math.floor(h1.length / 2) - 1,
        result_num = 1,
        err = null;
    if (h1.length === 0) return;
    for (var i = start; 0 <= i; i--) {
        var below = 2 * (i + 1) - 1;
        if (h1[i] < h1[below] || h1[i] < h1[below + 1]) {
            err = 'ERROR at ' + i;
            break;
        }
    }
    this.record.check[result_num] = err || 'OK';

    var h2 = this.heap_sorted,
        vp = null, // value prev
        result_num = 2,
        err = null;
    if (h2.length === 0) return;
    for (var i = h2.length - 1; 0 <= i; i--) {
        var vc = h2[i]; // value current
        if (vp !== null && vc < vp) {
            err = 'ERROR at ' + i;
            break;
        }
        vp = vc;
    }
    this.record.check[result_num] = err || 'OK';
};

TestHeap.prototype.print = function (chars) {
    var dots  = this.ary.length > chars ? ', ...' : '',
        text = [
        '<p>number of elements (' + this.ary.length + ')<br>' +
            this.ary.slice(0, chars).join(', ') + dots,
        '<p>heap initial : check (' + this.record.check[1] + '), ' + 
            'time (' + this.record.time[1] + 'ms)<br>' +
            this.heap_init.slice(0, chars).join(', ') + dots,
        '<p>heap sorted : check (' + this.record.check[2] + '), ' + 
            'time (' + this.record.time[2] + 'ms)<br>' +
            this.heap_sorted.slice(0, chars).join(', ') + dots
        ];
    document.write(text.join(''));
};

var re1 = location.search.match(/len=(\d+)/),
    re2 = location.search.match(/print=(\d+)/),
    len = re1 ? re1[1] : 1000,
    chars = re2 ? re2[1] : Math.ceil(80 / (Math.log(len) / Math.log(10) + 1)),
    test = new TestHeap(len);
test.build2();
test.sort1();
test.check();
test.print(chars);
