/* coding: utf-8 */

(function () {
    var m = kob.loc.match(/(\d{6})\/(\d{2})\.html/);
    if (m)
        kob.ymd = m[1] + m[2];
})();

// set the location of current articles in data.js
(function () {
    for (var i = 0, len = kob.post.length; i < len; i ++) {
        if (String(kob.post[i][0]) === kob.ymd) {
            kob.loc_in_post = i;
            break;
        }
    }
})();

// top navi
(function () {
    var loc = kob.loc_in_post,
        ins = 2;
    var func2 = function (loc, move, ins) {
        var s = kob.ce('span');
            a = kob.ce('a');
            p = kob.post[loc + move],
            m = move === -1 ? '◀' : '▶', // &#9664;, &#9654;
            t = move === -1 ? 'Prev' : 'Next';

        p[0] = String(p[0]);
        a.className = 'top_navi';
        a.href = kob.pth + 'blog/' + p[0].substring(0, 6) + '/' +
            p[0].substring(6, 8) + '.html';
        // a.title = t + ' : ' + p[1];
        kob.tt(a, m + m);
        kob.ap(s, a);
        kob.tt(s, '|');
        var c = kob.id('top_link').childNodes[ins];
        c.parentNode.insertBefore(s, c);
    };
    if (loc > 0) {
        func2(loc, -1, ins);
        ins++;
    }
    if (loc < kob.post.length - 1)
        func2(loc, 1, ins);
})();

// header 1
(function () {
    // prepare metadata of current post
    var p = kob.post[kob.loc_in_post],
        d1 = kob.ce('div');
    d1.id = 'header_in_info';

    // date
    var s = kob.ce('span');
    s.id = 'header_in_date';
    kob.tt(s, kob.ymd_sep(kob.ymd));
    kob.ap(d1, s);

    // tag
    if (p) { // avoid error on local before updating data.js
        for (var i = 0, len = p[2].length; i < len; i++) {
            var str = p[2][i],
                lnk = kob.ce('a');
            lnk.className = 'header_in_tags';
            lnk.href = kob.pth + '?word=' + encodeURIComponent(str);
            kob.tt(lnk, '[' + str + ']');
            kob.ap(d1, lnk);
        }
    }

    // prepare the link tag to show history
    // This element has no text so not displayed by default.
    var his = kob.ce('a');
    his.id = 'history_link';
    his.href = 'javascript:void(0)';
    his.onclick = function () {
        var h = kob.id('history');
        h.className = (h.className === 'nodisp' ? '' : 'nodisp');
    };
    his.title = 'show / hide detail';
    kob.ap(d1, his);
    kob.ap(kob.id('header_in'), d1);
})();

// header 2
(function () {
    // title
    // use title tag in each page, while list page use metadata.
    // these two titles might differ.
    var d2 = kob.ce('div'),
        title = kob.adjustSpace(document.title);

    /* insert line break into title : omitting
    if (Math.min(screen.width, innerWidth) - parseInt(kob.right_width) > 600) {
        var delim = { '：' : '：<br>' };
        for (var key in delim) {
            var val = delim[key];
            if (title.indexOf(key) > 0) {
                title = title.split(key).join(delim[key]);
                break;
            }
        }
    }
    */
    d2.id = 'header_in_title';
    d2.innerHTML = title;
    kob.ap(kob.id('header_in'), d2);
})();

// set main content
(function () {
    var func = function (tags) {
        for (var i = 0, len = tags.length; i < len; i++) {
            var n = tags[i],
                t = n.childNodes;

            if (t.length !== 1)
                continue;
            if (t[0].nodeType !== 3)
                continue;
            n.innerHTML = kob.adjustSpace(t[0].nodeValue);
        }
    };

    // adjust text using querySelector on IE8 or after
    // c.f.
    // https://developer.mozilla.org/ja/docs/Web/API/Document/querySelector
    // https://developer.mozilla.org/ja/docs/Web/API/Element/querySelectorAll
    var fg = kob.articleFgm;
    
    if (fg.querySelectorAll) {
        func(fg.querySelectorAll('span'));
        func(fg.querySelectorAll('a, td, li, h1, .h1'));
    }
    var chd = fg.childNodes,
        len = chd.length,
        m = kob.id('main');
    for (var i = 0; i < len; i++)
        kob.ap(m, chd[i].cloneNode(true));

    // TODO: apply below codes to fg
    var chd = m.childNodes,
        len = chd.length;
    for (i = 0; i < len; i++) {
        var n = chd[i];
        if (n.nodeType !== 3)
            continue;
        var v = n.nodeValue;

        // delete \n at first, last
        if (i === 0)
            v = v.replace(/^\n+/, '');
        else if (i === len - 1)
            v = v.replace(/\n+$/, '');
        var s = kob.ce('span'),
            t = kob.adjustSpace(v);

        // on IE7,8 all series of \n in the source have been changed to a space.
        // so "replace" above has no action and there is no <BR>.
        // c.f. http://d.hatena.ne.jp/elm200/20080229/1204251582
        var h = t.replace(/^\n/, '').replace(/\n/g, '<br>');
        s.innerHTML = h
        n.parentNode.replaceChild(s, n);
    }
})();
    
// set notice for IE7,8
(function () {
    if (! kob.ua.match(/MSIE [78]/))
        return;
    var d = kob.ce('div');
    d.id = 'forIE78'
    d.innerHTML = '※IE7・8では段落内の改行が消えるなど<br>見づらい所があります';
    kob.ap(kob.id('header'), d);
})();

// insert contents' title
(function () {
    var u = kob.id('contents');
    if (u === null)
        return;

    var d = kob.ce('div');
    d.id = 'contents_pre';
    kob.tt(d, 'Contents');
    u.parentNode.insertBefore(d, u);
    
    u.style.marginLeft = '';
})();

// create new content table
(function () {
    var d1 = kob.id('contents_new_wrap'),
        h1s = kob.id('main').getElementsByTagName('h1'),
        len = h1s.length;
    if (d1 === null || len === 0)
        return;

    var d2 = kob.ce('div'),
        u = document.createElement('ul');
    d2.id = 'contents_new_pre';
    u.id = 'contents_new';
    kob.tt(d2, 'Contents');
    kob.ap(d1, d2);
    kob.ap(d1, u);
    
    var proc_before_after = function (tg) {
        var tgs = d1.getElementsByTagName(tg);
        if (d1 === null)
            return;
        for (var i = 0, len = tgs.length; i < len; i++) {
            var a = tgs[i].cloneNode(true),
                li = document.createElement('li');
            kob.ap(li, a);
            kob.ap(u, li);;
        }
    };
    proc_before_after('before');

    for (var i = 0; i < len; i++) {
        var h1 = h1s[i],
            chd = h1.childNodes,
            j = i + 1,
            anc = document.createElement('a'),
            lnk = document.createElement('a'),
            li = document.createElement('li');
        anc.name = j;
        h1.parentNode.insertBefore(anc, h1);
        for (var k = 0; k < chd.length; k++)
            kob.ap(i > 0 ? lnk : li, chd[k].cloneNode());
        if (i > 0) {
            lnk.href = '#' + j;
            kob.ap(li, lnk);
        }
        kob.ap(u, li);
    }
    proc_before_after('after');
})();

// img
(function () {
    var spn = kob.ce('span'),
        div2 = kob.ce('div');
    spn.className = 'fa icon-zoom-in';
    div2.className = 'img_wrap small';

    var imgs = document.querySelectorAll('#main > img');
    for (var i = 0; i < imgs.length; i++) {
        var j = imgs[i],
            s = spn.cloneNode(),
            d = div2.cloneNode(),
            n = j.nextSibling;
        j.removeAttribute('style');
        j.onclick = s.onclick = function(e) { kob.resizeImg(e) };
        kob.ap(d, s);
        kob.ap(d, j);
        n.parentNode.insertBefore(d, n);
    }
})();

kob.resizeImg = function (e) {
    try {
        var t;
        if (e.tagName && e.tagName.match(/^((IMG)|(SPAN))$/i))
            // from "<tag onclick = functionName(this)>"
            t = e;
        else
            // from "onclick = function(e) { functionName(e) }"
            t = e ? e.target : window.event.srcElement;
        var p = t.parentNode,
            s = p.getElementsByTagName('span')[0],
            c = p.className;
    } catch(e) {
        return; // just in case
    }

    if (c.match(/large$/)) {
        p.className = c.replace(/large/, 'small');
        s.className = 'fa icon-zoom-in';
    } else {
        p.className = c.replace(/small/, 'large');
        s.className = 'fa icon-zoom-out';
    }
};

// callback function from below
kob.setSource = function (elm) {
    var wrap = kob.ce('div'),
        code = kob.ce('pre'),
        lab  = kob.ce('div'),
        ank = kob.ce('a');

    kob.ap(wrap, ank);
    kob.ap(wrap, lab);
    kob.ap(wrap, code);

    elm.className = 'nodisp';
    elm.parentNode.insertBefore(wrap, elm);

    wrap.className = 'code_wrap';
    code.className = 'code_body';
    code.id = 'code' + elm.index.toString(); // for selection
    lab.className  = 'code_label';

    if (elm.height !== ''){
        var h = elm.height.toString();
        if (kob.ua.match(/MSIE/)) h = h.replace('px', '') + 'px';
        code.className += ' scroll';
        code.style.height = h;
    }

    if (elm.sourceCode) { // API
        lab.fname = elm.path;
        var str = elm.sourceCode;
        if (! kob.ua.match(/MSIE 7/)) str = str.replace(/\r\n/g, '\n');
        kob.tt(code, str);
        ank.name = lab.fname;
    } else { // IFRAME
        lab.fname = elm.src.match(/([^\/]+)\.txt$/)[1];
        var n = elm.contentWindow.document.getElementsByTagName('pre')[0];
        if (typeof n.childNodes === 'undefined' ||
                n.childNodes == null || n.childNodes.length === 0) {
            console.log('???');
            // on IE8. check later
        } else {
            kob.tt(code, n.childNodes[0].nodeValue);
            ank.name = lab.fname + '.txt';
        }
    }
    lab.label = lab.fname;
    var m = lab.fname.match(/[^\/]+$/); // just in case
    kob.tt(lab, (m === null) ? lab.fname : m[0]);

    var lab_in = kob.ce('span'),
        a = kob.tagCodeSelect('Select');
    a.file = lab.fname;
    a.code_index = code.id;
    a.onclick = function (e) {
        try {
            var lab = kob.ymd + ' ' + e.target.parentNode.file;
        } catch (e) {
            return;
        }
        if (lab)
            select_doc(kob.id(e.target.parentNode.code_index));
    };

    if (kob.ua.match(/MSIE [78]/) === null)
        kob.ap(lab_in, a);

    if (elm.sourceCode) { // API
        var lnk1 = kob.url_pref + 'bitbucket.org/kenpg/' + elm.repo +
                   '/raw/master/' + elm.path,
            lnk2 = 'bitbucket.org/kenpg/' + elm.repo + '/src/master/'
                + elm.path;
    } else {
        var m1 = kob.loc.replace(/[^\/]+$/, ''),
            m2 = elm.src.replace(m1, ''),
            m3 = (m1 + m2).match(/\/blog\/.+$/)[0],
            lnk1 = elm.src,
            lnk2 = 'bitbucket.org/kenpg/kenpg.bitbucket.org/src/master' + m3;
    }

    var a = kob.tagCodeSelect('Rawtext');
    a.href = lnk1;
    a.target = '_blank';
    kob.ap(lab_in, a);

    var a = kob.tagCodeSelect('Bitbucket');
    a.file = lab.fname;
    a.href = kob.url_pref + lnk2;
    a.target = '_blank';
    kob.ap(lab_in, a);
    kob.ap(lab, lab_in);

    /* added 2016.6.5 */
    if (!document.body.getBoundingClientRect())
        return;
    var padNum = 17,
        width_lab = lab.getBoundingClientRect().width - padNum,
        width_code = code.getBoundingClientRect().width;
    
    if (width_lab === width_code || !getComputedStyle)
        return;

    var style_lab = getComputedStyle(lab),
        style_code = getComputedStyle(code);
    if (width_lab > width_code)
        code.style.width = width_lab + 'px';
    else
        lab.style.width = (width_code - padNum) + 'px';
};

// build link, list
(function () {
    var lists = document.querySelectorAll('link1, list1');
    for (var i = 0; i < lists.length; i++) {
        var list = lists[i],
            items = list.innerHTML.replace(/(^\s+)|(\s+$)/g, '').split(/\n/);
        while(list.hasChildNodes())
            list.removeChild(list.firstChild);
        items.forEach(function(item) {
            var d = kob.ce('div');
            kob.ap(list, d);
            if (list.tagName.match(/link/i)) {
                d.innerHTML = item;
                var a1 = d.querySelector('a').innerHTML;
                d.querySelector('a').innerHTML = '&raquo;' + '&ensp;' + a1;
            } else {
                d.innerHTML = '&bull;' + '&ensp;' + item;
            }
        });
    }
})();

// convert iframe to pre
(function () {
    var f = kob.id('main').getElementsByTagName('iframe');
    for (var i = 0, len = f.length; i < len; i++) {
        var j = f[i],
            s = j.src,
            p = '\\.((' + [
            'ahk',
            'bas',
            'bat',
            'cmd',
            'conf',
            'css',
            'html',
            'js',
            'py',
            'R',
            'sh',
            'sql',
            'svg',
            'vbs'].join(')|(') + '))\\.txt$';
        j.index  = i;
        if (new RegExp(p, 'i').exec(s) === null) continue;
        kob.onload(j, j, function(e) {
            var t = e ? e.target : window.event.srcElement;
            kob.setSource(t); // c.f. above
        });
        j.src = s; // required sometimes
    }
})();

// return link tag of code selection, righit-click and goto Bitbucket
kob.tagCodeSelect = function (lab) {
    var a = kob.ce('a'),
        s = kob.ce('span');
    if (kob.ua.match(/MSIE 7/) === null) { // unavailable on IE7
        var fa = {
            'Select': 'icon-paste',
            'Rawtext': 'icon-download',
            'Bitbucket': 'icon-bitbucket-sign'},
            i = kob.ce('i');
        i.className = fa[lab];
        kob.ap(a, i);
    }
    kob.tt(s, lab);
    kob.ap(a, s);
    a.href = 'javascript:void(0)';
    if (lab === 'Select')
        a.style.marginLeft = 0;
    return a;
};

// wrap bare pre
(function () {
    var codes = document.querySelectorAll('#main > pre');
    for (var i = 0;  i < codes.length; i++) {
        var c = codes[i],
            n = c.nextSibling,
            d = kob.ce('pre_wrap'),
            s = kob.ce('span');
        s.className = 'icon-expand select';
        s.title = 'Select';
        s.onclick = function(e) {
            var t = e ? e.target : window.event.srcElement;
            if (t)
                select_doc(t.parentNode);
        };
        kob.ap(d, s);
        kob.ap(d, c);
        n.parentNode.insertBefore(d, n);
    }
})();

function select_doc (doc) {
    // http://stackoverflow.com/questions/3169786/clear-text-selection-with-javascript

    var r = document.createRange();
    if (! doc || ! r)
        return;

    var sel = window.getSelection ? window.getSelection() : document.selection,
        selected = sel.anchorNode === doc;

    if (sel) {
        if (sel.removeAllRanges)
            sel.removeAllRanges();
        else if (sel.empty)
            sel.empty();
        if (! selected) {
            r.selectNodeContents(doc);
            sel.addRange(r);
        }
    }
}

// This is new funcion converting original "bb" tag to pre same as above
(function () {
    var b = kob.id('main').getElementsByTagName('bb');
    if (b.length === 0) return;

    kob.bbObj = {}; // path: {index : int, repo : string }
    for (var i = 0, len = b.length; i < len; i++) {
        var elm = b[i],
            repo = elm.getAttribute('repo'),
            path = elm.getAttribute('path');
        if (!repo || !path) return;

        // display loading
        if (! kob.ua.match(/MSIE [78]/)) {
            elm.className = 'code_loading';
            elm.style.background = kob.code_color;
            kob.tt(elm, 'loading from Bitbucket...');
        }
        var u = kob.url_pref + 'api.bitbucket.org/1.0/repositories/kenpg/' + 
                repo + '/src/master/' + path + '?callback=kob.bbCallback';
        kob.bbObj[path] = { 'index' : i, 'repo' : repo };
        kob.as(u, null, true);
    }
    kob.bbCallback = function (json) {
        var obj = kob.bbObj[json.path],
            ind = obj.index,
            elm = kob.id('main').getElementsByTagName('bb')[ind],
            h = elm.getAttribute('height');
        elm.index = ind;
        elm.sourceCode = json.data;
        elm.path = json.path;
        elm.height = h ? h : '';
        elm.repo = obj.repo;
        kob.setSource(elm); // process same as IFRAME
    };
})();

// add link and message to video
(function () {
    var c = kob.id('main').getElementsByTagName('video');
    if (c.length === 0) return;
    
    var canPlay = (typeof HTMLVideoElement === 'undefined' ||
                   typeof c[0].canPlayType === 'undefined' ||
                   c[0].canPlayType('video/webm') === '') ? false : true;

    for (var i = 0, len = c.length; i < len; i++) {
        var v = c[i],
            s = v.src;
        if (typeof s === 'undefined') s = v.getAttribute('src');
        // for Win7-Safari 5.1.4

        var w = kob.ce('div'),
            a = kob.ce('a');
        w.className = 'video_wrap';
        a.className = 'video_link';
        a.href = s;
        kob.tt(a, '≫ Link : ' + s.match(/[^\/]+$/)[0]);

        if (!canPlay) {
            var d = kob.ce('div');
            d.className = 'video_none';
            d.innerHTML = 'Unable to play WebM on this browser.<br>' +
                'Please download movie and play it with a media player.';
            kob.ap(w, d);
            // v.className = 'nodisp'; // somehow useless on IE10
            v.style.display = 'none';
        }
        kob.ap(w, v.cloneNode(true));
        kob.ap(w, a);
        v.parentNode.replaceChild(w, v);
    }
})();

// start retriving histroies
(function () {
    // 2016.06
    // limit to pages with bb tags, e.g. blog/201505/13.html
    var bbs = document.querySelectorAll('#main bb');
    if (bbs.length === 0)
        return;

    // prepare data object
    kob.histObj = {'data': [], 'data1end': false, 'data2end': false};

    // callback function adding file histories
    kob.histCallBack_1 = function (json) { kob.procHist(json, 1) };
    kob.histCallBack_2 = function (json) { kob.procHist(json, 2) };

    // retrive only a sub folder of the article
    // no effect even if adding parameter 'limit=1'
    kob.as(
        kob.url_bapi + 'kenpg.bitbucket.org/filehistory/master' +
        '/blog/' + kob.ymd.substring(0, 6) + '/' +
        kob.ymd.substring(6, 8) + '/?callback=kob.histCallBack_1', null, true);

    // get the histry of a path (excluding filename) of the first <bb> tag
    // assuming that there is no other place to check history
    var b = bbs[0];
    try {
        p = b.getAttribute('path').replace(/[^\/]+$/, ''),
        u = kob.url_bapi + b.getAttribute('repo') +
            '/filehistory/master/' + p +
            '?callback=kob.histCallBack_2';
    } catch (e) {
        kob.histObj.data2end = true;
        return;
    }
    kob.as(u, null, true);
})();

// processing file histories
kob.procHist = function (json, mode) {
    var obj = {};
    for (var i = 0, len1 = json.length; i < len1; i++) {
        var j = json[i],
            f = j.files,
            m = j.message;
        if (m.match(/^.+\n/) !== null) { // just in case
            m = m.match(/^(.+)\n/)[1];   // use only the first row
        }
        for (var k = 0, len2 = f.length; k < len2; k++) {
            var path = f[k].file;
            if (!obj[path]) obj[path] = {};
            obj[path][j.timestamp] = m;
        }
    }
    if (mode === 1) kob.histToHtml_1(obj);
    if (mode === 2) kob.histToHtml_2(obj);
};

kob.histToHtml_1 = function (obj) {
    var y = kob.ymd,
        p = 'blog/' + y.substring(0, 6) + '/' + y.substring(6, 8);

    // HTML
    // reconsidering...
    // kob.histToHtml_3(obj, p + '.html');

    // IFRAME
    var ifr = kob.tg('iframe');
    for (var i = 0, len = ifr.length; i < len; i++) {
        var m = ifr[i].src.match(/[^\/]+$/);
        if (!m) continue; // just in case
        kob.histToHtml_3(obj, p + '/' + m[0]);
    }
    kob.histObj.data1end = true;
    kob.histToHtml_4();
};

kob.histToHtml_2 = function (obj) {
    var b = kob.tg('bb');
    for (var i = 0, len = b.length; i < len; i++) {
        kob.histToHtml_3(obj, b[i].getAttribute('path'));
    }
    kob.histObj.data2end = true;
    kob.histToHtml_4();
};

kob.histToHtml_3 = function (obj, path) {
    var o = obj[path];
    if (!o) return ;

    var ary = [];
    for (var k in o) ary.push(k);
    if (ary.length === 1) return;
    
    var t_rev = ary.sort().reverse()[0], // get the last date of revision
        y_rev = t_rev.substring(0, 10).replace(/-/g, '');
    if (y_rev <= kob.ymd) return;

    kob.histObj.data.push([y_rev, path, o[t_rev]]);
};

kob.histToHtml_4 = function () {
    var obj = kob.histObj;
    if (!obj.data1end || !obj.data2end || obj.data.length === 0) return;
    
    var y = kob.ymd,
        ary = obj.data.sort().reverse(),
        ylb = ary[0][0],
        h1 = kob.id('history_link'),
        h2 = kob.ce('div'),
        pref = 'blog/' + y.substring(0, 6) + '/' + y.substring(6, 8);

    h2.id = 'history';
    h2.className = 'nodisp';
    kob.ap(kob.id('header_in'), h2);

    if (y.substring(0, 4) === ylb.substring(0, 4)) {
        ylb = parseInt(ylb.substring(4, 6), 10).toString() + '/' + 
              parseInt(ylb.substring(6, 8), 10).toString();
    } else {
        ylb = kob.ymd_sep(ylb);
    }
    kob.tt(h1, ylb + ' Revised');
    h1.style.display = 'inline';
    
    for (var i = 0, len = ary.length; i < len; i++) {
        var j = ary[i],
            ymd = kob.ymd_sep(j[0]),
            lab1 = j[1],
            lab2 = lab1; // for simple filepath

        kob.tt(h2, ymd);
        
        if (lab1 === pref + '.html') {
            var s1 = kob.ce('span');
            lab2 = j[2];
            kob.tt(s1, lab2);
            kob.ap(h2, s1);
        } else {
            var s1 = kob.ce('a');
            if (lab1.indexOf(pref + '/') === 0) {
                lab2 = lab1.replace(pref + '/', '');
            }
            s1.href = '#' + lab2;
            kob.histToHtml_5(lab2, ymd);
            var m = lab2.match(/[^\/]+$/); // just in case
            kob.tt(s1, m === null ? lab2 : m[0]);
            kob.ap(h2, s1);

            var s2 = kob.ce('span');
            kob.tt(s2, '（' + j[2] + '）');
            kob.ap(h2, s2);
        }
        kob.br(h2);
    }
};

// change link color to revised files
kob.histToHtml_5 = function (lab, ymd) {
    if (!document.getElementsByClassName) return;
    var d1 = document.getElementsByClassName('code_label');
    for (var i = 0, len = d1.length; i < len; i++) {
        var j = d1[i];
        if (lab === j.label || lab === j.label + '.txt') {  
            var a = j.getElementsByTagName('a')[2];
            a.style.color = kob.link_hover_color;
            a.title = ymd + ' Revised';
            return;
        }
    }
};

// footer
(function () {
    var f = kob.id('footer'),
        p_prev,
        p_next;
    if (kob.loc_in_post > 0) {
        p_prev = kob.post[kob.loc_in_post - 1];
    }
    if (kob.loc_in_post < kob.post.length) {
        p_next = kob.post[kob.loc_in_post + 1];
    }

    var url_pre = kob.pth + 'blog/',
        ary = [
            [p_next, '&#8658; Next : '], 
            [p_prev, '&#8656; Prev : ']
        ];
    for (var i = 0, len = ary.length; i < len; i++) {
        var ob = ary[i][0];
        if (typeof ob === 'undefined') continue;
        ob[0] = String(ob[0]);
        var lnk = kob.ce('a');
        lnk.href = url_pre +
            ob[0].replace(/^(\d{6})(\d{2})$/, '$1/$2') + '.html';
        lnk.innerHTML = kob.adjustSpace(ob[1])
        f.innerHTML += ary[i][1];
        kob.ap(f, lnk);
        kob.tt(f, '（' + kob.ymd_sep(ob[0]) + '）');
        kob.br(f);
    }
    
    /* TODO: add links to related tags
    var tags = kob.id('r_tag');
    console.log(tags.innerHTML);
    var tags = kob.cl('header_in_tags');
    console.log(tags.length);
    */
})();

// check bottom space
kob.rightBottomSpace = function () {
    if (typeof document.body.getBoundingClientRect === 'undefined' ||
        kob.id('footer') === null ||
        kob.id('right') === null) return;
    var fBottom = kob.id('footer').getBoundingClientRect().bottom,
        rBottom = kob.id('right').getBoundingClientRect().bottom;
    return fBottom < rBottom;
};

// right gadget
(function () {
    if (kob.ymd === '20150501' || !Object.keys) return;
    if (kob.rightBottomSpace()) return;

    var d = kob.id('r_link').cloneNode(),
        btn = kob.ce('input');
    
    d.id = 'r_bgmContainer';
    d.childNodes = [];
    d.innerHTML = '&#x266c;&nbsp;My recommended music videos on YouTube<br>';

    btn.id    = 'btn1';
    btn.type  = 'button';
    btn.value = 'Show';
    btn.onclick = function (e) {
        //if (e.target.value === 'Show' && !kob.local && !kob.test)
        //    ga('send', 'event', 'music_show', kob.ymd);
        kob.setVideoAndPlayer(e);
    };
    kob.ap(d, btn);
    kob.ap(kob.id('right'), d);
})();

// start right gadget
kob.setVideoAndPlayer = function (e) {
    var t = e.target;
    if (typeof t === 'undefined') return; // just in case
    
    var act = t.value;
    t.value = 'Close';
    t.onclick = function () { kob.endPlayer() };

    if (act === 'Show Again') {
        var c = t.parentNode.childNodes;
        for (var i = 0, len = c.length; i < len; i++) {
            if (typeof c[i].style !== 'undefined' && c[i].id !== 'btn4') {
                c[i].style.visibility = 'visible';
            }
        }
        return;
    }
    kob.as('videos.js');
};

// callback function when metadata has loaded
kob.bgm = function (obj) {
    kob.bgm_list = obj;
    kob.bgm_2();
    kob.bgm_3(obj.common);
};

// prepare button and container for videos and player
kob.bgm_2 = function () {
    var b1 = kob.id('btn1'),
        b2 = b1.cloneNode();
    b2.id  = 'btn2';
    b2.onclick = function () {
        // if (!kob.local && !kob.test)
        //    ga('send', 'event', 'music_reorder', kob.ymd);
        kob.bgm_reorder();
    };
    b2.value = 'Reorder';
    kob.ap(kob.id('r_bgmContainer'), b2);

    var d = kob.ce('div');
    d.id = 'r_videos';
    kob.ap(kob.id('r_bgmContainer'), d);

    // console.log(kob.ua.match(/mobile/));
    /* remove temporarily to modify

    var b3 = kob.ce('div');
    b3.id = 'btn3';
    b3.title = ['Use a simple JSON file like',
        '{',
        '"ID of your favorite video 1" : "Title 1",',
        '"ID of your favorite video 2" : "Title 2", , ,',
        '}',
        'Title can be empty.',
        'Reorder above returns to my selection.'].join('\n');
    var lnk = kob.ce('a');
    kob.tt(lnk, '♬ Use your list');
    
    lnk.href = 'javascript:void(0)';
    lnk.onclick = function () {
        // if (!kob.local && !kob.test)
        //    ga('send', 'event', 'music_readfile', kob.ymd);
        kob.id('btn4').click();
    };
    kob.ap(b3, lnk);
    kob.ap(kob.id('r_bgmContainer'), b3);

    var bf = kob.ce('input');
    bf.id = 'btn4';
    bf.onchange = function (e) { kob.setUserList(e.target.files[0]) };
    bf.type = 'file';
//    bf.style.display = 'none'; // no action on Mobile Safari 4
    kob.ap(kob.id('r_bgmContainer'), bf);
    */
};

// set videos from metadata 
kob.bgm_3 = function (obj, from_local) {
    var ary = Object.keys(obj);
    if (!from_local) {
        ary = ary.sort(function () { return Math.random() - 0.5 });
        ary = ary.slice(0, 5); // set the maximum number of videos
    }
    kob.videoAry = ary;

    var d1 = kob.id('r_videos');
    for (var i = 0, len = ary.length; i < len; i++) {
        var m = kob.ce('div');
        m.loc = i;
        m.onclick = function (e) {
            kob.startVideo(e);
            if (kob.local || kob.test)
                return;
            // ga('send', 'event', 'music_play', e.target.title);
        };
        m.vid = kob.videoAry[i];
        m.title = obj[m.vid];
        m.className = 'r_video_images';
        m.style.backgroundImage = 'url("https://i.ytimg.com/vi/' +
            m.vid + '/default.jpg")';
        kob.ap(d1, m);

        if (i > 0 && kob.rightBottomSpace() && !from_local) {
            kob.rm(m);
            kob.videoAry = ary.slice(0, i);
            break;
        }
        if (kob.rightBottomSpace()) {
            window.scroll(0, m.getBoundingClientRect().bottom);
        }
    }
};

// onclick btn2
kob.bgm_reorder = function () {
    kob.rm(kob.id('player')); // kob.id('player').style.display = 'none';

    var v = kob.id('r_videos').childNodes,
        all_vid = Object.keys(kob.bgm_list.common);
    all_vid.sort(function () { return Math.random() - 0.5 });

    if (typeof kob.bgm_showed === 'undefined') {
        kob.bgm_showed = kob.videoAry;
    } else {
        kob.bgm_showed.push(kob.videoAry);
    }
    if (all_vid.length - kob.bgm_showed.length < v.length) {
        kob.bgm_showed = [];
    }

    var func = function () {
        var showed = kob.bgm_showed;
        for (var j = 0, len1 = all_vid.length; j < len1; j++) {
            var cand = all_vid[j];
            var exists = false;
            for (var k = 0, len2 = showed.length; k < len2; k++) {
                if (cand === showed[k]) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                showed.push(cand);
                return cand;
            }
        }
    };
    
    kob.videoAry = [];
    for (var d = null, i = 0, len = v.length; i < len; i++) {
        d = v[i];
        d.vid = func();
        d.style.backgroundImage =
            'url("' + kob.url_pref +'i.ytimg.com/vi/' + d.vid + '/default.jpg")';
        d.title = kob.bgm_list.common[d.vid];
        kob.videoAry.push(d.vid);
    }
};

// onclick btn3
// TODO: change to use kob.onload
kob.setUserList = function (file) {
    var r = new FileReader();
    r.onload = function (e) {
        var js = eval('(' + e.target.result + ')');
        kob.bgm_2();
        kob.bgm_3(js, true); // the second argument is for this function only
    };
    r.readAsText(file);
};

// onclick video's thumbnail
kob.startVideo = function (e) {
    try { var t = e.target; } catch (e) { return; } // just in case

    var loc = t.loc, // a subscript of videoAry
        temp = kob.videoAry,
        list = temp.slice(loc + 1).concat(temp.slice(0, loc)).join(','),
        p = kob.id('player');

    // set options for embed player
    // in reference to http://qiita.com/stakei1/items/ccae2e0029b2cbd7e33b
    var opt = ['?autoplay=1',
        'autohide=1',
        'color=white',
        'disablekb=1',        // disable keyboard shorcut
//      'fs=1',               // fullscreen button but no effect in this player
        'iv_load_policy=3',   // no annotation
        'loop=0',
//      'enablejsapi=1',      // meaning of this isn't clear so far
//      'playerapiid=player', // same as above
        'playlist=' + list,
        'playsinline=0',      // for iOS but not confirmed yet
        'rel=0'].join('&');
    kob.rm(p);
    p = kob.ce('iframe');
    p.id = 'player';
    p.src = kob.url_pref + 'www.youtube.com/embed/' + t.vid + opt;
    t.parentNode.insertBefore(p, t.parentNode.childNodes[t.loc]);
};

// end right gadget
kob.endPlayer = function () {
    var ids = ['r_videos', 'btn2', 'btn3'];
    for (i = 0, len = ids.length; i < len; i++) {
        var d = kob.id(ids[i]);
        if (d !== null) d.style.visibility = 'hidden';
    }
    var b = kob.id('btn1');
    b.value = 'Show Again';
    b.onclick = function(e) { kob.setVideoAndPlayer(e) };
};

// for auto scrolling on local
// if (kob.local) {
if (false && kob.local) { // insufficient yet
    window.addEventListener('unload', function() {
        document.cookie = kob.id('right').getBoundingClientRect().top;
    });
    window.addEventListener('load', function() {
        var rTop = kob.id('right').getBoundingClientRect().top,
            moveHeight = document.cookie - rTop;
        window.scrollBy(0, moveHeight * -1);
    });
}
// alert('IE10 なぜかここに来てない. 全体は普通に実行されてるのに');