@echo off
chcp 65001

set pgdir=D:/AppsPortable/PostgreSQLPortable-9.4
set ctl=%pgdir%/bin/pg_ctl
set dir=%pgdir%/data
set pid=%dir%/postmaster.pid

:: set environmental variables for PostGIS Raster
set GDAL_DATA=../gdal-data
set POSTGIS_GDAL_ENABLED_DRIVERS=ENABLE_ALL
set POSTGIS_ENABLE_OUTDB_RASTERS=1

:: set environmental variables for PL/Python3
set pythonhome=D:/AppsPortable/WinPython/WinPython-32bit-3.3.5.8/python-3.3.5
set path=%pythonhome%

:: set environmental variables for PL/R
set r_home=D:/AppsPortable/R-Portable/3.1.2
set path=%path%;%pgdir%/bin;%pgdir%/lib;%r_home%/bin/i386

:: indicate Python and its version
echo.
python -V

:: indicate R and its version
echo.
R --version

:: start PostGIS
if exist "%pid%" (
    echo PostgreSQL might have been started.
) else (
    "%ctl%" -D "%dir%" start
)

:: start QGIS
set qgs=example_postgis.qgs
set qgs=%~dp0%qgs%
D:
cd \AppsPortable\QGIS-Portable-CH\bin
call o4w_env.bat
path %OSGEO4W_ROOT%\apps\qgis\bin
set QGIS_PREFIX_PATH=%OSGEO4W_ROOT:\=/%/apps/qgis
set conf=--configpath %OSGEO4W_ROOT%\.qgis2
set proj=--project "%qgs%"
start "" %OSGEO4W_ROOT%\bin\qgis-bin.exe %conf% %proj%

:ask
set /p inp="press [R] to restart PostgreSQL, [Q] to quit : "
if "%inp%"=="R" (
    "%ctl%" -D "%dir%" -w restart
    goto ask
) else if "%inp%"=="Q" (
    "%ctl%" -D "%dir%" -w stop
    goto :eof
)
goto ask
