@echo off

:: example
set pdf=20150530_Slide.pdf
set page_start=10
set page_end=20
set file_pre=
set gs="Ghostscript/bin/gswin32c.exe"
set opt=-sDEVICE=png16m -r150 -dTextAlphaBits=4

setlocal enabledelayedexpansion
for /L %%i in (%page_start%, 1, %page_end%) do (
    set pages=-dFirstPage=%%i -dLastPage=%%i
    set png=-sOutputFile=%file_pre%%%i.png
    %gs% -q -dBATCH -dNOPAUSE %opt% !pages! !png! %pdf%
    echo !png!
)
pause
