/* coding: utf-8 */

// settings
kob.base_color = 'rgb(220, 235, 250)';
kob.back_color = 'White';
kob.code_color = 'WhiteSmoke';
kob.link_color = 'RoyalBlue';
kob.link_color2 = 'SlateBlue';
kob.link_color3 = 'DarkBlue';
kob.link_hover_color = 'crimson';
kob.list_color = 'WhiteSmoke';
kob.head_color = 'CornFlowerBlue';
kob.top_height = '100px';
kob.right_width = '210px';
kob.image_init_width = '200px';
kob.right_padLeft = 20;

kob.addstyle = function(obj) {
    if (kob.ua.match(/MSIE 6/)) return; // IE6 does not support addRule

    var sheet,
        ie78 = (kob.ua.match(/MSIE [78]/) === null) ? false : true;
    if (ie78) {
        sheet = document.createStyleSheet();
    } else {
        var style = kob.ce('style');
        kob.ap(kob.tg('head')[0], style);
        sheet = style.sheet;
    }

    // "Object.keys" is unavailable on IE8 and before
    for (var k in obj) {
        var str = obj[k].join(';');
        if (sheet.addRule){
            // for browsers which doesn't support CSS pseudo-class
            if (ie78 && k.match(/:nth-child/))
                continue;
            sheet.addRule(k, str);
        } else if (sheet.insertRule) {
            sheet.insertRule(k + '{' + str + '}', 0);
        }
    }
};

kob.addstyle({
    'body': [
        'background-color:' + kob.base_color,
        'font-family:' + (kob.ua.match(/Windows/) ? 'Meiryo, ' : '') +
            'sans-serif'],
    '#top': [
        'height:' + kob.top_height],
    '#header': [
        'background:' + kob.back_color,
        'border-bottom: solid 1px ' + kob.back_color,
        'margin-right:' + kob.right_width],
    '#header_in': [
        'border-left: solid 5px ' + kob.head_color],
    '#main': [
        'background:' + kob.back_color,
        'margin-right:' + kob.right_width],
    '#footer': [
        'background:' + kob.back_color,
        'border-right: 5px solid ' + kob.back_color,
        'margin-right:' + kob.right_width],
    '#right': [
        'padding-left: ' + kob.right_padLeft + 'px',
        'top:' + kob.top_height,
        'width:' + (parseInt(kob.right_width) - kob.right_padLeft) + 'px'],
    '.r_subtitle': [
        'background:' + kob.back_color,
        'border-top: solid 1px ' + kob.link_color,
        'margin-left:' + (-1 * kob.right_padLeft) + 'px',
        'padding-left:' + (kob.right_padLeft - 0) + 'px'
        ],
    '#r_thanks': [
        'border-top: solid 4px ' + kob.back_color],
    '#right a': [
        'color:' + kob.link_color3],
    '#top a, #header a, #main a, #footer a': [
        'color:' + kob.link_color],
    '#top a:hover, #right a:hover': [
        'background-color:' + kob.link_color2,
        'color: ' + kob.back_color],
    '#header a:hover, #main a:hover, #footer a:hover': [
        'background-color:' + kob.base_color],
    '.img_wrap > span:hover': [
        'background-color:' + kob.link_color,
        'color:' + kob.back_color],
});

if (kob.idx) {
    kob.addstyle({
        '.article_list.bgcolor': [
            'background:' + kob.list_color],
        '#header #img_toggle': [
            'background:' + kob.head_color,
            'color: ' + kob.back_color],
        '#search': [
            'top:' + kob.top_height,
            'right:' + kob.right_width]
    });
} else {
    var sh = 'inset 0 0 5px ' + kob.base_color,
        shadows = [sh, sh, sh, sh].join(',');
    kob.addstyle({
        'pre': [
            'background:' + kob.code_color],
        'span.select': [
            'color:' + kob.link_color],
        'span.select:hover': [
            'background:' + kob.link_color,
            'color:' + kob.back_color],
        'h1, .h1': [
            'border-left: solid 5px ' + kob.head_color, 
            'margin:' + (kob.ua.match(/MSIE [78]/) ? '25': '5') + 'px 0 1em'],
        '.ps > span': [
            // 'border-bottom: solid 1px ' + kob.link_hover_color, 
            'color:' + kob.link_hover_color],
        '.img_wrap > span': [
            'color: ' + kob.link_color],
        '.img_wrap.small > img':[
            'width: ' + kob.image_init_width],
        '#history_link': [
            'background:' + kob.head_color],
        '#history_link:hover': [
            'background-color:' + kob.base_color],
        '#history': [
            'background:' + kob.base_color],
        '#contents_pre, #contents_new_pre': [
            'background:' + kob.list_color],
        '#forIE78 > span': [
            'background:' + kob.base_color],
        '#r_bgmContainer': [
            'border-top: dotted 4px ' + kob.back_color],
        '.r_video_images': [
            'box-shadow:' + shadows]
    });
    if (kob.ua.match(/chrome/i))
        kob.addstyle({
            'pre': [
                'font-size: 1.125em'],
            '.code_label': [
                'padding-bottom: 3px'],
            '.code_label i ': [
                'font-size: 0.875em',
                'margin-right: 0.125em']
        });
    else if (kob.ua.match(/firefox/i))
        kob.addstyle({
            'pre': [
                'font-size: 0.8125em'],
            '.code_label': [
                'font-size: 0.875em',
                'padding-bottom: 3px'],
            '.code_label i ': [
                'margin-right: 0.125em']
        });
    else if (kob.ua.match(/msie/i))
        kob.addstyle({
            '#search_text': [
                'border: lightgray solid 1px']
        });
}

// add link to atom feed and css url to header
(function () {
    var h = document.getElementsByTagName('head')[0],
        lnk = document.createElement('link');
    lnk.rel = 'alternate';
    lnk.type = 'application/atom+xml';
    lnk.title = 'Atom';
    lnk.href = 'http://kenpg.php.xdomain.jp/atom.xml';
    h.appendChild(lnk);

    var pre = kob.pth + 'blog/',
        css = [pre + 'main.css',
            pre + 'sub_' + (kob.idx ? 'list' : 'article') + '.css'],
        fa_url = '/font-awesome/3.2.1/css/font-awesome.min.css';
        // TODO: update to the latest version
    if (kob.local || kob.test)
        css.push(kob.pth + 'lib' + fa_url);
    else {
        var ref = document.referrer.replace(/^https*:\/\//i, '');
        css.push(kob.url_pref + 'cdnjs.cloudflare.com/ajax/libs' + fa_url);
        /* suspended
        css.push('//' + 
            // 'css3test.php.xdomain.jp/css.php'    // to 2016.8.30
            'kenpg.php.xdomain.jp/temporary.css'  // from 2016.8.31
                + '?' + (new Date).getTime() + (ref ? '&' + ref : ''));
        */
    }
    css.forEach(function(c) {
        var lnk = document.createElement('link');
        lnk.rel = 'stylesheet';
        lnk.href = c;
        h.appendChild(lnk);
    });
})();

// set width
// c.f.
// http://qiita.com/sukobuto/items/51bb471d2c8ecdb5407b
// http://www.hanano-ya.jp/html/1800
// http://memo.morelents.com/tablet-useragent/
kob.setWidth = function () {
    var iwidth = window.innerWidth || document.documentElement.clientWidth ||
            document.body.clientWidth;
    if (kob.ua.match(/mobile/i) &&
            kob.ua.match(/iPad/) === null &&
            Math.min(screen.width, iwidth) < 500) {
            // Android 4.0 Mobile Safari : screen.width > innerWidth
        var m = kob.ce('meta'),
            w = 400;
        m.setAttribute('name', 'viewport');
        m.setAttribute('content',
            'target-densitydpi=device-dpi, width=' + w + ', user-scalable=yes');
        kob.ap(kob.tg('head')[0], m);
        kob.mobileWidth = true;
    }

    var rw = kob.right_width,
        ww = iwidth - parseInt(rw),
        smallWidth = ww < 600 ? true : false,
        noRight = (kob.mobileWidth || ww < 420) ? true : false,
        marRight = ['margin-right: ' + (noRight ? 0 : rw)],
        header = marRight.concat(),
        dispBlock = ['display: ' + (noRight ? 'none' : 'block')];

    kob.addstyle({
        '#logo': ['max-width : ' + (noRight ? '100%' : '420px')],
        '#header': header,
        '#main': marRight,
        '#footer': marRight,
        '#oldsite_wrap': dispBlock,
        '#right': dispBlock,
        '#top_link': [
            'right:' + (noRight ? '1em' : '0')
        ],
        '#top_link a': [
            'font-size:' + (noRight ? '0.75em' : '0.875em'),
            'padding-left:' + (noRight ? '0.25em' : '0.5em'),
            'padding-right:' + (noRight ? '0.25em' : '0.5em')
        ]
    });

    if (kob.idx) {
        kob.addstyle({
            '#search': [
                'position: ' + (noRight ? 'relative' : 'absolute'),
                'right: ' + (noRight ? 0 : kob.right_width),
                'text-align: ' + (noRight ? 'left' : 'right'),
                'top: ' + (noRight ? 0 : kob.top_height)]
        });
    } else if (noRight) {
        kob.addstyle({
            'pre': [
                'width: 100%']
        });
    }
};

kob.setHeight = function () {
    /*
    work in progress
    try {
        var r = kob.id('right');
        if (getComputedStyle(kob.id('right')).display === 'none')
            return;
        var f_bottom = kob.id('footer').getBoundingClientRect().bottom,
            rect = r.getBoundingClientRect(),
            r_bottom = rect.bottom;
    } catch(e) {
        return;
    }
        alert(f_bottom + ' / ' + r_bottom);
    if (f_bottom < r_bottom)
        kob.addstyle({
            '#right': [
                'height: ' + (rect.height - (r_bottom - f_bottom)) + 'px'
            ]
        });
    */
};

kob.setWidth();
kob.setHeight();
window.onresize = function() {
    kob.setWidth();
    kob.setHeight();
};
