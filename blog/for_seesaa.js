(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-42334728-1', 'seesaa.net');
ga('send', 'pageview');

(function(u){
    var h = document.getElementsByTagName('head')[0],
        k = document.createElement('link'),
        ref = document.referrer;
    k.rel = 'stylesheet';
    k.href =
        // '//css3test.php.xdomain.jp/css.php' +
        '//kenpg.php.xdomain.jp/temporary.css'      // from 2016.8.31
         + '?' + (new Date).getTime() + (ref ? '&' + ref : '');
    h.appendChild(k);
})();
