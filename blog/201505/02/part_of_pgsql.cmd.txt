:: This code is a part of "pgsql.cmd" in PostgreSQL Portable 9.4.1
:: URL : http://sourceforge.net/projects/postgresqlportable/files/9.4/
:: File: PostgreSQLPortable-9.4.1.paf.exe
:: By:   Gareth Flowers
:: License: MIT License

:: set up postgres variables
set PGSQL=%APPBASE%\App\PgSQL
set PGDATA=%APPBASE%\Data\data
set PGLOG=%APPBASE%\Data\log.txt
set PGLOCALEDIR=%PGSQL%\share\
set PGDATABASE=postgres
set PGUSER=postgres
set PATH=%PGSQL%\bin;%PATH%

:: initialise a new database on first use
if not exist "%PGDATA%" (
    echo.
    echo Initialising database for first use, please wait...
    "%PGSQL%\bin\initdb" -U %PGUSER% -A trust -E utf8 --locale=C >nul
)
