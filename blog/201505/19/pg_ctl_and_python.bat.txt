@echo off

set ctl=App/PgSQL/bin/pg_ctl
set dir=Data/data
set pid=%dir%/postmaster.pid

:: set environmental variables for PL/Python3
set pythonhome=%~d0/AppsPortable/WinPython/WinPython-32bit-3.3.5.8/python-3.3.5
set path=%pythonhome%

:: indicate Python and its version
echo.
python -V
echo.

if exist "%pid%" (
    echo postmaster.pid exists and PostgreSQL have been started possibly.
    goto ask
) else (
    "%ctl%" -D "%dir%" -w start
)

:ask
set /p inp="press [R] to restart PostgreSQL, [Q] to quit : "
if "%inp%"=="R" (
    "%ctl%" -D "%dir%" -w restart
    goto ask
) else if "%inp%"=="Q" (
    "%ctl%" -D "%dir%" -w stop
    goto :eof
)
goto ask
