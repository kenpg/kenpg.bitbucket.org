-- create dummy data
CREATE VIEW test.dummy AS
SELECT concat('2015-05-', day) :: date AS "ymd",
       round(random() :: numeric * 100, -1) AS "foovar",
       CASE WHEN random() < 0.5 THEN 0 ELSE 1 END AS "event"
FROM generate_series(1, 31) "day";

-- save to a temporary file
COPY (SELECT * FROM test.dummy)
TO 'R:/to_plr.tsv' (DELIMITER E'\t', FORMAT csv, HEADER TRUE);

-- pseudo DO
DO $_d_$
BEGIN
PERFORM test.plr_do($_r_$
    dat = read.delim('R:/to_plr.tsv', stringsAsFactors=F)
    head(dat, 20)
$_r_$);
END $_d_$;
