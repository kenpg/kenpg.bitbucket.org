@echo off

set ctl=bin/pg_ctl
set dir=data
set pid=%dir%/postmaster.pid

:: set an environmental variable for PostGIS Raster
:: this path should be an absolute path or a relative one from lib folder
set GDAL_DATA=../gdal-data

if exist "%pid%" (
    echo postmaster.pid exists and PostgreSQL have been started possibly.
    goto ask
) else (
    "%ctl%" -D "%dir%" -w start
)

:ask
set /p inp="press [R] to restart PostgreSQL, [Q] to quit : "
if "%inp%"=="R" (
    "%ctl%" -D "%dir%" -w restart
    goto ask
) else if "%inp%"=="Q" (
    "%ctl%" -D "%dir%" -w stop
    goto :eof
)
goto ask
