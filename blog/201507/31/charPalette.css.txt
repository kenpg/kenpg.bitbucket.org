#charInput {
    padding: 5px;
}

#charButton {
    margin-left: 0.25em;
    margin-right: 1em;
}

#charPalette {
    border-collapse: collapse;
}

#charPalette td {
    border: solid 1px lightblue;
    text-align: center;
    padding: 5px 5px 4px;
}

#charPalette tr:first-child td:first-child {
    border-style: none;
}

#charPalette tr:first-child, tr td:first-child {
    color: gray;
    font-family: monospace;
}

#charPalette tr:nth-child(n+2) td:nth-child(n+2) {
    cursor: pointer;
    font-size: 1.25em;
}

#charPalette .clicked {
    background: lightblue;
}

#charNavi {
    white-space: nowrap;
}

#charNavi button {
    font-size: 0.7em;
    margin-right: 0.5em;
}

.enlarge {
    border: solid lightblue 1px;
    float: left;
    margin-bottom: 10px;
    margin-right: 10px;
    padding: 0 10px 3px;
    position: relative;
    white-space: nowrap;
}

.enlargeClear {
    background: lightblue;
    cursor: pointer;
    color: white;
    font-size: 1.5em;
    position: absolute;
    right: 0;
    top: 0;
}

.enlargeChar {
    font-size: 4em;
    margin-right: 10px;
}

.enlargeLabel {
    font-family: monospace;
}

#clearEnlarge {
    clear: both;
}        

#charNotes {
    text-align: left;
}
