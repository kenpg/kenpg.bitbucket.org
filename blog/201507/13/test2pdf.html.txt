<!DOCTYPE html><html>
<head>
<meta charset="UTF-8">
<style>
body {
  margin: 0;
}
div {
  background: #ABCDEF;
  color: white;
  font-size: 40px;
  height: calc(96px * 4 - 10px * 2);
  line-height: 2;
  padding: 10px 20px;
  width: calc(96px * 8 - 20px * 2);
}
</style>
</head>
<body>
<div>
  background: #ABCDEF;<br>
  font-size: 40px; line-height: 2;<br>
  height: calc(96px * 4);<br>
  width: calc(96px * 8);<br>
</div>
</body>
</html>
