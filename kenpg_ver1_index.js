/* coding: utf-8 */

// main object literal
var kob = {
    loc : location.href,
    ua  : navigator.userAgent,
    ap: function (x, y) { x.appendChild(y) },
    ce: function (x) { return document.createElement(x) },
    id: function (x) { return document.getElementById(x) },
    rm: function (x) { if (x !== null) x.parentNode.removeChild(x) },
    tg: function (x) { return document.getElementsByTagName(x) },
    tt: function (x, y) { x.appendChild(document.createTextNode(y)) }
};

// add property to main object
kob.idx = kob.loc.match(/\/blog\//) ? false : true;
kob.pth = kob.idx ? '' : '../../';
kob.local = kob.loc.indexOf('file:///') === 0 ? true : false;
kob.url_pref = kob.local ? 'https://' : '//';
kob.test = kob.loc.match(/:\/\/kenpg\.bitbucket\.io/i) ? false : true;
kob.url_bapi = kob.url_pref + 'bitbucket.org/api/1.0/repositories/kenpg/';

// execution on loaded
// c.f. http://sscrisk.hatenablog.com/entry/20120826/1345986280
// c.f. http://blog.yuhiisk.com/archive/2014/12/20/dynamic-loading-and-complete-processing-of-script.html
kob.onload = function(ob1, ob2, func) {
    var done = false;
    ob1.onload = ob2.onreadystatechange = function(e) {
        if (!done && (!this.readyState ||
                this.readyState === 'loaded' ||
                this.readyState === 'complete')) {
            done = true;
            func(e);
            this.onload = this.onreadystatechange = null;
        }
    };
};

// add script
kob.as = function (js, func, url) {
    var funcTemp = function() {
        var s = kob.ce('script');
        if (func)
            kob.onload(s, s, func);
        s.type = 'text/javascript';
        s.src  = (url ? '' : kob.pth + 'blog/') + js;
        kob.ap(kob.tg('head')[0], s);
    };
    if (kob.ua.match(/MSIE 10/) && js.match(/^sub/)) {
        var tid = setInterval(function() {
            if (kob.common_end_for_ie) {
                clearInterval(tid);
                funcTemp();
            }
        }, 100);
    } else {
        funcTemp();
    }
};

// utility for date formating
kob.ymd_sep = function (ymd) {
    return String(ymd).replace(/(\d{4})(\d{2})(\d{2})/, '$1/$2/$3');
};

// utility for adding BR tag
kob.br = function (x, y) {
    if (typeof y === 'undefined') y = 1;
    for (var i = 0; i < y; i++) kob.ap(x, kob.ce('br'));
};

kob.cl = function (x) {
    if (document.getElementsByClassName) {
        return document.getElementsByClassName(x)
    } else {
        // for IE8 or before
        var ele = document.getElementsByTagName('*'),
            len = ele.length,
            ary = [];
        for (var i = 0; i < len; i++) {
            var j = ele[i];
            if (j.className === x) ary.push(j);
        }
        return ary;
    }
};

/*| 
|*| now not used
|*|
// shrink text limitting by height, as a try
// doesn't support surrogate pairs
kob.shrinkTextHeight = function (doc, lim, mark) {
    if (doc.offsetHeight <= lim) return;

    var chd = doc.childNodes;
    if (!mark) mark = '…';
    for (var i = chd.length - 1; i >= 0; i--) {
        var nd = chd[i],
            name = nd.nodeName.toLowerCase(),
            done = false;
        if (name === '#text') {
            var str = nd.nodeValue;
            for (var j = str.length; j >= 0; j--) {
                nd.nodeValue = str.substring(0, j);
                if (doc.offsetHeight <= lim) {
                    nd.nodeValue += mark;
                    if (doc.offsetHeight <= lim) {
                        done = true;
                        break;
                    } else {
                        nd.nodeValue = str.substring(0, j);
                    }
                }
            }
        } else if (name == 's1'){
            kob.rm(nd);
        } else {
            // unexpected so far
        }
        if (done)
            break;
    }
}

kob.adjustRecentPosts = function () {
    if(!document.querySelectorAll)
        return;
    var nodes = document.querySelectorAll('#r_recent_ul li a');
    for (var i = 0, len = nodes.length; i < len; i++) {
        kob.shrinkTextHeight(nodes[i], 50, '…');
    }
};

kob.lastProc = function () {
    if (kob.local) {
        document.title = '(Local) ' + document.title;
    } else if (kob.test) {
        document.title = '(Test) ' + document.title;
    }
    kob.adjustRecentPosts();
};
|*|
|*| now not used
|*/

kob.as('data.js', function () {
    var fns = function() {
        kob.as('style.js', function() {
            kob.as('main.js', function() {
                var js = 'sub_' + (kob.idx ? 'list' : 'article') + '.js';
                kob.as(js, function() {
                    // kob.lastProc();
                });
            });
        });
    };
    try {
        var test = document.body.childNodes.length;
    } catch (e) {
        // console.log('Error');
    }
    if (test)
        fns();
    else 
        kob.onload(window, document, function() { fns(); });
});
